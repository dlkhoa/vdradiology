package application;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.text.ParseException;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Main extends Application {

	@SuppressWarnings("unused")
	private Stage primaryStage;

	public static void main(String[] args) {
		launch(args);
	}

	private static final String internalFile = System.getenv("public") + File.separator
			+ "SettingFileRelaisImagerie.txt";

	@Override
	public void start(Stage primaryStage) throws ParseException {

		// String WebParam
		// ="DBSWIN://categorie=Capteur;NomPatient=Smith;PrenomPatient=Brian;Date_naissance=15011996;SECU=23;LieuNaissance=Smalltown;Adresse=Smallstreet
		// 33;FAX=01234;Medecin=Prof.Dr.;Sexe=m;Identifiant_patient=76543;";
		// c'est �a il a modifi� dans le fichier regarder encore/
		// Smith;Brian;15.1.1996;23;Smalltown;Smallstreet
		// 33;01234/5678;Prof.Dr.;m;76543;
		// String WebParam = getParameters().getNamed().get("WebParam");
		String WebParam = getParameters().getUnnamed().toString();
		System.out.println(WebParam);

		/*
		 * String line = WebParam.split("//")[1]; String Param =
		 * line.split(";")[0].replaceAll( "\\/" , "" ); String MyCateg =
		 * Param.split("=")[1]; String NomPatient = line.split(";")[1].split("=")[1];
		 * String PrenomPatient = line.split(";")[2].split("=")[1]; String
		 * Date_naissance = line.split(";")[3].split("=")[1]; String Identifiant_patient
		 * = line.split(";")[4].split("=")[1];
		 */

		// Map<String, List<String>> List_Categ_Marque = new HashMap<String,
		// List<String>>();
		Map<String, String> List_Marque_Path = new HashMap<String, String>();

		try {
			BufferedReader SettingFileBuffer = new BufferedReader(new FileReader(internalFile));
			String Myline;
			while ((Myline = SettingFileBuffer.readLine()) != null) {
				if (Myline.trim().isEmpty())
					continue;

				String[] MyFileLine = Myline.split("=");
				switch (MyFileLine[1]) {
				case "CSImaging":
					List_Marque_Path.put("CSImaging", MyFileLine[2]);
					break;
				case "MyRay":
					List_Marque_Path.put("MyRay", MyFileLine[2]);
					break;
				case "Romexis":
					List_Marque_Path.put("Romexis", MyFileLine[2]);
					break;
				case "Sirona":
					List_Marque_Path.put("Sirona", MyFileLine[2]);
					break;
				case "Scanora":
					List_Marque_Path.put("Scanora", MyFileLine[2]);
					break;
				case "DBSWIN":
					List_Marque_Path.put("DBSWIN", MyFileLine[2]);
					break;
				case "RSVImaging":
					List_Marque_Path.put("RSVImaging", MyFileLine[2]);
					break;
				case "CliniView":
					List_Marque_Path.put("CliniView", MyFileLine[2]);
					break;
				case "VixWin":
					List_Marque_Path.put("VixWin", MyFileLine[2]);
					break;
				case "Vatech":
					List_Marque_Path.put("Vatech", MyFileLine[2]);
					break;

				default:
					System.exit(0); // System.out.println("Exit002"); System.exit(0);
				}
			}
			SettingFileBuffer.close(); // donne moi 5 minutes je te reviens rapidement ok

		} catch (IOException e) {
			e.printStackTrace();
		}

		/*
		 * for (Map.Entry <String,String> mapentry : List_Marque_Path.entrySet()) {
		 * //System.out.println(mapentry.getKey()+"------"+mapentry.getValue()); }
		 */

		////////////////////////////////////////// Ancienne version (Afichage des softs
		////////////////////////////////////////// par categorie)
		////////////////////////////////////////// ///////////////////////////////////////

		/*
		 * while ((Myline=SettingFileBuffer.readLine())!=null) { String[] MyLine =
		 * Myline.split("=");
		 * 
		 * switch(MyLine[0]) { case "Capteur":
		 * List_Categ_Marque.put("Capteur",Systeme_imagerie_Capteur); break; case
		 * "Panoramique": List_Categ_Marque.put("Panoramique",Systeme_imagerie_Pano);
		 * break; case "Camera":
		 * List_Categ_Marque.put("Camera",Systeme_imagerie_Camera); break; default :
		 * System.exit(0); } }
		 */

		/*
		 * switch(MyCateg) { case "Capteur":
		 * List_Categ_Marque.put("Capteur",Systeme_imagerie_Capteur); break; case
		 * "Panoramique": List_Categ_Marque.put("Panoramique",Systeme_imagerie_Pano);
		 * break; case "Camera":
		 * List_Categ_Marque.put("Camera",Systeme_imagerie_Camera); break; default :
		 * System.out.println("Exit001"); System.exit(0); }
		 * 
		 */

		/////////
		// File SettingFile = new
		///////// File("C:\\Users\\selah\\eclipse-workspace\\VisioImagerie\\SettingFile.txt");

		/*
		 * BufferedReader SettingFileBuf = new BufferedReader(new
		 * FileReader("C:\\Windows\\SettingFileRelaisImagerie.txt"));
		 * 
		 * String Mylines; while ((Mylines = SettingFileBuf.readLine())!= null) {
		 * String[] MyFileLines = Mylines.split("=");
		 * 
		 * switch(MyFileLines[1]) { case "Kodak":
		 * List_Marque_Path.put("Kodak",MyFileLines[2]); break; case "PlanMeca":
		 * List_Marque_Path.put("PlanMeca",MyFileLines[2]); break; case "Sirona":
		 * List_Marque_Path.put("Sirona",MyFileLines[2]); break; case "Scanora":
		 * List_Marque_Path.put("Scanora",MyFileLines[2]); break; case "Durr":
		 * List_Marque_Path.put("Durr",MyFileLines[2]); break; case "VisioDent":
		 * List_Marque_Path.put("VisioDent",MyFileLines[2]); break; case "CliniView":
		 * List_Marque_Path.put("CliniView",MyFileLines[2]); break;
		 * 
		 * default : System.out.println("Exit002"); System.exit(0); } }
		 * SettingFileBuf.close();
		 */

		GridPane Grid_RelaisImagerie = new GridPane();
		// Label Categ = new Label("Categorie : " +MyCateg );
		Button Btn_Kodak = new Button("CSImaging");
		Btn_Kodak.setPrefSize(250, 40);
		Button Btn_VixWin = new Button("VixWin");
		Btn_VixWin.setPrefSize(250, 40);
		Button Btn_Cefla = new Button("MyRay");
		Btn_Cefla.setPrefSize(250, 40);
		Button Btn_CliniView = new Button("CLINIVIEW");
		Btn_CliniView.setPrefSize(250, 40);
		Button Btn_PlanMeca = new Button("ROMEXIS");
		Btn_PlanMeca.setPrefSize(250, 40);
		// Btn_PlanMeca.getChildren().AddAll(Identifiant_patient,NomPatient,
		// PrenomPatient,Date_naissance);
		Button Btn_Cirona = new Button("SIRONA");
		Btn_Cirona.setPrefSize(250, 40);
		Button Btn_Vatech = new Button("VATECH");
		Btn_Vatech.setPrefSize(250, 40);
		Button Btn_Scanora = new Button("SCANORA");
		Btn_Scanora.setPrefSize(250, 40);
		Button Btn_Durr = new Button("DBSWIN");
		Btn_Durr.setPrefSize(250, 40);
		Button Btn_Visiodent = new Button("RSVImaging");
		Btn_Visiodent.setPrefSize(250, 40);
		Button Btn_QuickVision = new Button("QuickVision"); // N'est pas fonctionnel !
		Btn_QuickVision.setPrefSize(250, 40);

		////////////// Cas o� le praticien dispose de plusieurs Systeme d'imagerie
		////////////// /////////////////////////////////
		int k = 0;
		for (Map.Entry<String, String> mapentry : List_Marque_Path.entrySet()) {
			switch (mapentry.getKey()) {
			case "CSImaging":
				Grid_RelaisImagerie.add(Btn_Kodak, 8, k + 3);
				break;
			case "Romexis":
				Grid_RelaisImagerie.add(Btn_PlanMeca, 8, k + 3);
				break;
			case "Sirona":
				Grid_RelaisImagerie.add(Btn_Cirona, 8, k + 3);
				break;
			case "Vatech":
				Grid_RelaisImagerie.add(Btn_Vatech, 8, k + 3);
				break;
			case "Scanora":
				Grid_RelaisImagerie.add(Btn_Scanora, 8, k + 3);
				break;
			case "DBSWIN":
				Grid_RelaisImagerie.add(Btn_Durr, 8, k + 3);
				break;
			case "MyRay":
				Grid_RelaisImagerie.add(Btn_Cefla, 8, k + 3);
				break;
			case "RSVImaging":
				Grid_RelaisImagerie.add(Btn_Visiodent, 8, k + 3);
				break;
			case "CliniView":
				Grid_RelaisImagerie.add(Btn_CliniView, 8, k + 3);
				break;
			case "QuickVision":
				Grid_RelaisImagerie.add(Btn_QuickVision, 8, k + 3);
				break;
			case "VixWin":
				Grid_RelaisImagerie.add(Btn_VixWin, 8, k + 3);
				break;
			default:
				System.exit(0);
			}
			k++;
		}

		/////////////////////////// Action events on Button's Action
		/////////////////////////// ////////////////////

		Btn_Kodak.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("CSImaging", List_Marque_Path.get("CSImaging"), WebParam);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		});

		Btn_Cefla.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("MyRay", List_Marque_Path.get("MyRay"), WebParam);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		});

		Btn_CliniView.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("CliniView", List_Marque_Path.get("CliniView"), WebParam);
			} catch (ParseException e) {

				e.printStackTrace();
			}
		});

		Btn_PlanMeca.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("Romexis", List_Marque_Path.get("Romexis"), WebParam);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		});

		Btn_Cirona.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("Sirona", List_Marque_Path.get("Sirona"), WebParam);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		});

		Btn_Vatech.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("Vatech", List_Marque_Path.get("Vatech"), WebParam);
			} catch (ParseException e) {
				e.printStackTrace();
			}

		});

		Btn_Scanora.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("Scanora", List_Marque_Path.get("Scanora"), WebParam);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		});
		Btn_Durr.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("DBSWIN", List_Marque_Path.get("DBSWIN"), WebParam);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		});
		Btn_Visiodent.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("RSVImaging", List_Marque_Path.get("RSVImaging"), WebParam);
			} catch (ParseException e) {
				e.printStackTrace();
			}
		});
		Btn_QuickVision.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("QuickVision", List_Marque_Path.get("QuickVision"), WebParam);

			} catch (ParseException e) {
				e.printStackTrace();
			}
		});
		Btn_VixWin.setOnAction(actionEvent -> {
			URIHandler UriHand = new URIHandler();
			try {
				UriHand.URI_Handler("VixWin", List_Marque_Path.get("VixWin"), WebParam);

			} catch (ParseException e) {
				e.printStackTrace();
			}
		});

		primaryStage.setWidth(400);
		primaryStage.setHeight(400);

		Grid_RelaisImagerie.setHgap(10);
		Grid_RelaisImagerie.setVgap(10);
		// Grid_RelaisImagerie.add(Categ, 1, 1);
		Scene scene = new Scene(Grid_RelaisImagerie, 400, 400);
		scene.getStylesheets().add(getClass().getResource("/application/resources/application.css").toExternalForm());
		primaryStage.setTitle("Menu Systemes imagerie");
		primaryStage.setScene(scene);

		if (List_Marque_Path.size() > 1) {
			primaryStage.show();

		}

		else {
			for (Map.Entry<String, String> mapentry : List_Marque_Path.entrySet()) {
				URIHandler UriHand = new URIHandler();
				UriHand.URI_Handler(mapentry.getKey(), mapentry.getValue(), WebParam);
			}

			System.exit(0);
		}

	}

}
