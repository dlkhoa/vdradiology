package application;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ChoixCateg extends Application
{
	private static final String internalFile = System.getenv("public") + File.separator + "SettingFileRelaisImagerie.txt" ;
	
	public Map<String,String> List_Exe_Map = new HashMap<String,String>();
	
	public void start(Stage primaryStage) throws IOException 
	{
		//System.out.println(internalFile);
		
		GridPane Grid_Choice = new GridPane();
		Image Icon = new Image( getClass().getResourceAsStream( "/application/resources/logo_visiodent2.jpg" ) ) ;
		Image Image_Capteur = new Image( getClass().getResourceAsStream( "/application/resources/capteur.jpg" ) ) ;				
		Image Image_Pano = new Image( getClass().getResourceAsStream( "/application/resources/panoramique.jpg" ) ) ;
		Image Image_Camera = new Image( getClass().getResourceAsStream( "/application/resources/camera.jpg" ) ) ; 		
		Image Image_Logo = new Image( getClass().getResourceAsStream( "/application/resources/logo_visiodent1.jpg" ) ) ;
		//ImageView Icon_ImageView = new ImageView(Icon);
		ImageView Capteur_ImageView = new ImageView(Image_Capteur);
		ImageView Pano_ImageView = new ImageView(Image_Pano);
		ImageView Camera_ImageView = new ImageView(Image_Camera);
		ImageView Logo_ImageView = new ImageView(Image_Logo);
		
		
		Capteur_ImageView.setFitHeight(200);
		Capteur_ImageView.setFitWidth(200);
		Pano_ImageView.setFitHeight(200);
		Pano_ImageView.setFitWidth(200);
		Camera_ImageView.setFitHeight(200);
		Camera_ImageView.setFitWidth(200);
		Logo_ImageView.setFitHeight(200);
		Logo_ImageView.setFitWidth(220);
		Button Btn_Capteur = new Button();
		Btn_Capteur.setGraphic(Capteur_ImageView);
		Btn_Capteur.setMinSize(200, 200);
		Button Btn_Pano = new Button();
		Btn_Pano.setGraphic(Pano_ImageView);
		Btn_Pano.setMinSize(200, 200);		
		Button Btn_Camera = new Button();
		Btn_Camera.setGraphic(Camera_ImageView);
		Btn_Camera.setMinSize(200,200);
		
			
		/*if(!SettingFile.exists())
			{			
				SettingFile.createNewFile();			
			  
			}*/
				
 		
		Grid_Choice.setStyle("-fx-background-color: AliceBlue;");
		

		//////////////////////Grid Param////////////////
		
		GridPane Grid_Param = new GridPane();
		String Categ = new String();		
		
		Button Btn_Previous = new Button("Pr�c�dent");
		Btn_Previous.setPrefSize(100, 40);
		Button Browse = new Button("Parcourir...");
		Browse.setPrefSize(100, 10);
		Button Btn_executer = new Button("Ex�cuter");
		//List <Object> List_Exe = new List<Object>;
		 
		
		Btn_executer.setAlignment(Pos.BASELINE_CENTER);
		Btn_executer.setPrefSize(100, 20);
		Label label = new Label("Logiciel");
		Label label_Category = new Label("");				
		Label label_DirChoser = new Label("R�pertoire de sauvegarde radios patients ");
		     // label_DirChoser.("patient");
		Label label_ExeChoser = new Label("Fichier executable ad�quat");
		Label labelChoixCateg  = new Label("Choix de la cat�gorie : ");
		//Label label1 = new Label(Categ);			
		FileChooser Chooser = new FileChooser();
		DirectoryChooser DirChoser = new DirectoryChooser(); 
		Button Browse_DirChoser = new Button("Parcourir...");		
		Browse_DirChoser.setPrefSize(100, 10);
		TextField textField = new TextField ();
		textField.setPrefSize(400, 10);
		TextField textField_PatDir = new TextField ();
		textField_PatDir.setPrefSize(400, 10);
		
		// for SIRONA
		Label label_ChooserSdx = new Label("Fichier mailbox (SLIDA) ");
		TextField textField_SdxFile = new TextField ();
		textField_SdxFile.setPrefSize(400, 10);
		FileChooser chooserSdx = new FileChooser();
		Button Browse_ChooserSdx = new Button("Parcourir...");		
		Browse_ChooserSdx.setPrefSize(100, 10);
		Label label_StationName = new Label("Nom du station ");
		TextField textField_StationName = new TextField ();
		textField_StationName.setPrefSize(400, 10);
		
		//ListView<String> listMarque = new ListView<String>();
		ObservableList<String> listCapteur =FXCollections.observableArrayList ("CSImaging", "RSVImaging","Romexis","DBSWIN","Scanora","CliniView","MyRay","VixWin", "Sirona", "Vatech");
		final ComboBox<String> comboBox = new ComboBox<String>(listCapteur);
		comboBox.setPrefSize(400, 10);
		comboBox.getSelectionModel().selectFirst();
		
		if(comboBox.getSelectionModel().getSelectedItem().equals("CSImaging")||comboBox.getSelectionModel().getSelectedItem().equals("MyRay")||comboBox.getSelectionModel().getSelectedItem().equals("VixWin"))
		{
			Grid_Param.add(label_DirChoser,0,4);
			Grid_Param.add(textField_PatDir,1,4);
			Grid_Param.add(Browse_DirChoser,2,4);				
		}
		
		comboBox.setOnAction((event) ->{
						
			Grid_Param.getChildren().remove(label_DirChoser);
			Grid_Param.getChildren().remove(textField_PatDir);
			Grid_Param.getChildren().remove(Browse_DirChoser);

			Grid_Param.getChildren().remove(label_ChooserSdx);
			Grid_Param.getChildren().remove(textField_SdxFile);
			Grid_Param.getChildren().remove(Browse_ChooserSdx);
			Grid_Param.getChildren().remove(label_StationName);
			Grid_Param.getChildren().remove(textField_StationName);

			if(comboBox.getSelectionModel().getSelectedItem().equals("CSImaging")||comboBox.getSelectionModel().getSelectedItem().equals("MyRay")||comboBox.getSelectionModel().getSelectedItem().equals("VixWin"))
			{
				Grid_Param.add(label_DirChoser,0,4);
				Grid_Param.add(textField_PatDir,1,4);
				Grid_Param.add(Browse_DirChoser,2,4);				
			}
			else if (comboBox.getSelectionModel().getSelectedItem().compareToIgnoreCase("SIRONA") == 0)
			{
				Grid_Param.add(label_ChooserSdx, 0, 4);
				Grid_Param.add(textField_SdxFile, 1, 4);
				Grid_Param.add(Browse_ChooserSdx, 2, 4);				
				Grid_Param.add(label_StationName, 0, 5);				
				Grid_Param.add(textField_StationName, 1, 5);				
			}
						
		});
		
		
		Grid_Choice.setHgap(7);
		Grid_Choice.setVgap(7);
		Scene scene = new Scene(Grid_Choice,400,400);
		
		Browse.setOnAction(actionEvent -> 
		{ 				
			File file = Chooser.showOpenDialog(primaryStage.getScene().getWindow());        // Ajouter un DirectoryChooser
		    if (file != null) 
		    {
		    	textField.setText(file.getPath());			         
		    } 
		});
		
		Browse_DirChoser.setOnAction(actionEvent -> 
		{ 				
			File file = DirChoser.showDialog(primaryStage.getScene().getWindow());        // Ajouter un DirectoryChooser
		    if (file != null) 
		    {
		    	textField_PatDir.setText(file.getPath());			         
		    } 
		});

		Browse_ChooserSdx.setOnAction(actionEvent -> 
		{ 				
			File fileSdx = chooserSdx.showOpenDialog(primaryStage.getScene().getWindow());
		    if (fileSdx != null) 
		    {
		    	textField_SdxFile.setText(fileSdx.getPath());			         
		    } 
		});

		File SettingFile = new File(internalFile);	
			
		Btn_executer.setOnAction(actionEvent -> 
		{ 					   			   
		   try 
		   {			   
			   		if( !SettingFile.exists() )
			   			SettingFile.createNewFile() ;				   					 			   		
			   		
			   		BufferedReader SettingFileBuffer = new BufferedReader(new FileReader(internalFile));
			   		boolean flag = false; 
			   		//String lines = "" ;
			   					   		
			   		//SettingFileBuffer.close();
			   		
			   		// Si le fichier n'est pas vide
			   		//if( ( lines = SettingFileBuffer.readLine() ) != null )
			   		if( SettingFileBuffer.readLine() != null )
					{
			   			String cap ;			   						   			
			   			SettingFileBuffer.close();
			   			SettingFileBuffer = new BufferedReader(new FileReader(internalFile));
				   		
			   			//Scanner input = new Scanner(new File("\""+SettingFile+"\""));
			   			
			   			StringBuffer stringBuffer = new StringBuffer();
			   			
			   			while ( ( cap = SettingFileBuffer.readLine() ) != null )     
			   			{			   			
			   				if( cap.trim().isEmpty() || cap.trim().indexOf( "=" ) == -1 )
			   					continue ;
			   				
			   				//System.out.println(cap);	
			   				stringBuffer.append( cap ) ;
			   				
			   				String [] MyLines = cap.split( "=" ) ;			   							   				
			   				
			   				if( MyLines[ 1 ].equals( comboBox.getSelectionModel().getSelectedItem() ) )
			   				{
			   					Alert alert = new Alert(AlertType.INFORMATION);
			   					alert.setTitle("!");
			   					alert.setHeaderText(null);
			   					alert.setContentText("Un logiciel existe pour ce systeme d'imagerie !");
			   					alert.showAndWait();			   								   					
			   					flag = true;
			   					break ;			   					
			   					
			   				}
			   		   	}			   			
			   			
						 
			   			if(!flag)
			   			   {
							FileWriter SettingFileWriter = new FileWriter(SettingFile,true);		   					
		   					SettingFileWriter.write(label_Category.getText()+"=");							
		   					SettingFileWriter.write(comboBox.getSelectionModel().getSelectedItem()+"=");
		   					if(comboBox.getSelectionModel().getSelectedItem().equals("CSImaging")||comboBox.getSelectionModel().getSelectedItem().equals("MyRay")||comboBox.getSelectionModel().getSelectedItem().equals("VixWin"))
		   					{
		   						SettingFileWriter.write(textField.getText()+";");
			   					SettingFileWriter.write(textField_PatDir.getText()+System.lineSeparator());
		   					}
		   					else if (comboBox.getSelectionModel().getSelectedItem().compareToIgnoreCase("SIRONA") == 0)
		   					{
		   						SettingFileWriter.write(textField.getText() + ";");
		   						SettingFileWriter.write(textField_SdxFile.getText() + ";");
			   					SettingFileWriter.write(textField_StationName.getText() + System.lineSeparator());
		   					}
		   					else
		   					{
		   						SettingFileWriter.write(textField.getText()+System.lineSeparator());
		   					}
		   					SettingFileWriter.close();
		   					Alert alert = new Alert(AlertType.INFORMATION);
		   					alert.setTitle("!");	
		   					alert.setHeaderText(null);
		   					alert.setContentText("Systeme d'imagerie ajout�!");
		   					alert.showAndWait();		   						   						
			   			}
			   		}
			   		///Si le fichier est vide
			   		else
			   		{			   			
			   			FileWriter SettingFileWriter = new FileWriter(SettingFile,true);
			   			SettingFileWriter.write(label_Category.getText()+"=");							
	   					SettingFileWriter.write(comboBox.getSelectionModel().getSelectedItem()+"=");
	   					if(comboBox.getSelectionModel().getSelectedItem().equals("CSImaging")||comboBox.getSelectionModel().getSelectedItem().equals("MyRay")||comboBox.getSelectionModel().getSelectedItem().equals("VixWin"))
	   					{
	   						SettingFileWriter.write(textField.getText()+";");
		   					SettingFileWriter.write(textField_PatDir.getText()+System.lineSeparator());
	   					}	   												
	   					else if (comboBox.getSelectionModel().getSelectedItem().compareToIgnoreCase("SIRONA") == 0)
	   					{
	   						SettingFileWriter.write(textField.getText() + ";");
	   						SettingFileWriter.write(textField_SdxFile.getText() + ";");
		   					SettingFileWriter.write(textField_StationName.getText() + System.lineSeparator());
	   					}
	   					else
	   					{
	   						SettingFileWriter.write(textField.getText()+System.lineSeparator());
	   					}
	   					SettingFileWriter.close();	   					   
	   					Alert alert = new Alert(AlertType.INFORMATION);
	   					alert.setTitle("Ajout");	
	   					alert.setHeaderText(null);
	   					alert.setContentText("Systeme d'imagerie ajout�");
	   					alert.showAndWait();
	   								   			
			   		}
			   		SettingFileBuffer.close();
			   		primaryStage.setScene(scene);
					primaryStage.setWidth(500);
					primaryStage.setHeight(520);
   					
		   } 
		   catch (IOException e) 
		   {		
			   e.printStackTrace();
		   }		   		   		   		   		  
		});			
			
		Grid_Param.add(label_ExeChoser,0 , 3);
		Grid_Param.add(Browse,2 , 3);
		Grid_Param.add(label, 0, 2);		
		Grid_Param.add(textField, 1,3);		
		Grid_Param.add(Btn_executer, 2, 10);
		Grid_Param.add(Btn_Previous, 2, 15);
		Grid_Param.setAlignment(Pos.BASELINE_CENTER);
		Grid_Param.add(comboBox, 1, 2);

		Grid_Param.setHgap(6);
		Grid_Param.setVgap(6);
		Scene scene_Param = new Scene(Grid_Param,400,400);
		scene_Param.getStylesheets().add(getClass().getResource("/application/resources/application.css").toExternalForm());
		primaryStage.setTitle("Parametrage "+Categ);
		
		primaryStage.setWidth(780);
		primaryStage.setHeight(400);
					
	/////////////////////////////////////////////////////grid param/////////////////////////////	 				
								
		Btn_Capteur.setOnAction(actionEvent -> 
		{	
			String Category = "Capteur";
			label_Category.textProperty().set(Category);
			primaryStage.setScene(scene_Param);
			primaryStage.setWidth(780);
			primaryStage.setHeight(400);			
		});
		
		Btn_Pano.setOnAction(actionEvent -> 
		{	
			String Category = "Panoramique";			
			label_Category.textProperty().set(Category);
			primaryStage.setScene(scene_Param);
			primaryStage.setWidth(780);
			primaryStage.setHeight(400);												
		});
		
		Btn_Camera.setOnAction(actionEvent -> 
		{	
			String Category = "Camera";			
			label_Category.textProperty().set(Category);
			primaryStage.setScene(scene_Param);
			primaryStage.setWidth(780);
			primaryStage.setHeight(400);						
		});						
		Grid_Choice.add(labelChoixCateg,2,1);			
		Grid_Choice.add(Btn_Capteur,2 , 3);
		Grid_Choice.add(Btn_Pano,2 , 4);		
		Grid_Choice.add(Btn_Camera,3 , 3);	
		Grid_Choice.add(Logo_ImageView, 3, 4);
		//Grid_Choice.setGridLinesVisible(true); Affiche les grilles
		
		
		
		
/////////////////////////////////////////////////Grid_Param///////////////////////////////
		Btn_Previous.setOnAction(actionEvent -> 
		{ 				
			primaryStage.setScene(scene);
			primaryStage.setWidth(500);
			primaryStage.setHeight(520);
			
			
						
		});
/////////////////////////////////////////////////Grid_Param///////////////////////////////		
		scene.getStylesheets().add(getClass().getResource("/application/resources/application.css").toExternalForm());		
		primaryStage.setTitle(" Indiquer l'emplacement de l'application imagerie");
		primaryStage.getIcons().add(Icon);
		primaryStage.setScene(scene);
		primaryStage.setWidth(500);
		primaryStage.setHeight(520);
		primaryStage.setResizable(false);
	
		primaryStage.show();
		
	}
	
	public Map<String,String> getMyMap()
	{
        return List_Exe_Map;
    }
	public static void main(String[] args) 
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle("!");
		alert.setHeaderText(null);
		alert.setContentText("ok 1 !");
		alert.showAndWait();			   								   					
		launch(args);

	}

}
