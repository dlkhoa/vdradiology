package application;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.net.URLDecoder;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.StringUtils;
import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

import com.sun.glass.ui.Application;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class URIHandler {

	void URI_Handler(String Marque, String Path, String Parameters) throws ParseException {

		// Path = "\"" + Path + "\"" ;
		/////////////////////////////////////////////////////////////////////////////////////////
		/*
		 * String regex = "^vsdimaging:\\/\\/[^\\s]*$";
		 * 
		 * Pattern pattern = Pattern.compile(regex); Matcher matcher =
		 * pattern.matcher(Marque);
		 * 
		 * while (matcher.find()) { System.out.println("Full match: " +
		 * matcher.group(0)); for (int i = 1; i <= matcher.groupCount(); i++) {
		 * System.out.println("Group " + i + ": " + matcher.group(i)); } }
		 */

		/*
		 * if( args.length == 0 ) return ;
		 * 
		 * String Uri = new String().join("", args).trim();
		 * 
		 * System.out.println(Uri);
		 * 
		 * Pattern uri_handler_regex = Pattern.compile("@vsdimaging://(.*)");
		 * 
		 * String params = uri_handler_regex.matcher(Uri).group(1).valueOf(true); if
		 * (params.endsWith("/")) { params = params.Remove(params.IndexOf("/")); }
		 */

		//////////////////////////////////////////////////////////////////////////////////////////////// c'est
		//////////////////////////////////////////////////////////////////////////////////////////////// quoi
		// Smith;Brian;15.1.1996;23;Smalltown;Smallstreet 33;01234/5678;Pof.Dr.;m;76543;
		/*
		 * String line = Parameters.split("//")[1]; String NomPatient =
		 * line.split(";")[1].split("=")[1]; String PrenomPatient =
		 * line.split(";")[2].split("=")[1]; String Date_naissance =
		 * line.split(";")[3].split("=")[1]; String NumSecu =
		 * line.split(";")[4].split("=")[1]; String ville =
		 * line.split(";")[5].split("=")[1]; String adresse =
		 * line.split(";")[6].split("=")[1]; String numero_fax =
		 * line.split(";")[7].split("=")[1]; String medecin =
		 * line.split(";")[8].split("=")[1]; String Sexe =
		 * line.split(";")[9].split("=")[1]; String Identifiant_patient =
		 * line.split(";")[10].split("=")[1];
		 */

		String ville = "";
		String adresse = "";
		String medecin = "";
		String numero_fax = "";
		String NumSecu = "";

		String decodedParameters = Parameters;
		try 
		{
			decodedParameters = URLDecoder.decode(Parameters, "UTF-8" );
		} catch (Exception e) {}

		String line = decodedParameters.split("//")[1];
		String NomPatient = line.split(";")[1].split("=")[1];
		String PrenomPatient = line.split(";")[2].split("=")[1];
		String Date_naissance = line.split(";")[3].split("=")[1];
		String Identifiant_patient = line.split(";")[4].split("=")[1];
		String Sexe = line.split(";")[5].split("=")[1].replace("]", "");

		String MyExe = "";
		switch (Marque) {
		case "RSVImaging":
			//// Formatage de la date en format dd/mm/yyyy
			SimpleDateFormat sm_VisioDent = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_VisioDent = sm_VisioDent.parse(Date_naissance);
			SimpleDateFormat sdfDestination_VisioDent = new SimpleDateFormat("dd/MM/yyyy");
			Date_naissance = sdfDestination_VisioDent.format(DateNaissance_VisioDent);

			File AthenaFile = new File("C:\\Windows\\Athena.ini");

			// java.util.prefs.Preferences prefs = new IniPreferences(ini); // permet de
			// lire dans un fichier .Ini

			// Librairie externe (ini4j) Import�e dans le projet, elle permet de lire et
			// ecrire dans un fichier ini
			Wini ini;
			try {
				if (!AthenaFile.exists())
					AthenaFile.createNewFile();

				ini = new Wini(AthenaFile); // On associe notre fichier Athena � l'objet Ini
				ini.put("PATIENT", "Number", Identifiant_patient); // Modification des atributs on acc�dant � la section
																	// (Ex:PATIENT)
				ini.put("PATIENT", "Name", NomPatient + " " + PrenomPatient);
				ini.put("PATIENT", "BirthDate", Date_naissance);
				ini.store();
			} catch (InvalidFileFormatException e1) {
				e1.printStackTrace();
			} catch (IOException e1) {

				e1.printStackTrace();
			}
			Path = "\"" + Path + "\"";
			MyExe = Path;
			break;
		case "CSImaging":
			String PatDirectoryNum = "";
			String PathExe = Path.split(";")[0];
			String PatientPathDirectory = Path.split(";")[1];
			File PatientsDirectory = new File(PatientPathDirectory);
			String Date_naissance_CSImaging = Date_naissance.replace("-", "/");

			if (Character.toLowerCase(Identifiant_patient.charAt(0)) == 'j') // teste si le premier caractere est un "j"
																				// => cela signifie qu'il s'agit d'un
																				// identifiant Jade
			{
				PatDirectoryNum = Identifiant_patient.substring(1); // PatDirectoryNum Contient le IDPatient sans la
																	// lettre "J"

				/*********************
				 * Regle creation de dossier patient jade
				 *********************/

				int i = 0;

				while (i < PatDirectoryNum.length() && PatDirectoryNum.charAt(i) == '0') {
					i++;
				}

				if (PatDirectoryNum.charAt(0) == '0') {
					PatDirectoryNum = PatDirectoryNum.substring(i);
				}

				////////////////////////////////////////////////////////////////////////

				// � garder en cas de changement de regle de la part de

				// int k = 0;

				/*
				 * for(int j = 1;j<Identifiant_patient.length();j++) {
				 * //if((Identifiant_patient.charAt(Identifiant_patient.length()-3)
				 * ==Identifiant_patient.charAt(j) && Identifiant_patient.charAt(j) == '0') ||
				 * (Identifiant_patient.charAt(Identifiant_patient.length()-4) ==
				 * Identifiant_patient.charAt(j) && Identifiant_patient.charAt(j)== '0')) //{
				 * k=i; //} if(Identifiant_patient.charAt(j) == '0' &&
				 * Identifiant_patient.charAt(j-1) == '0') { i=j; //enregistre la position du j
				 * } }
				 * 
				 * if(Identifiant_patient.charAt(Identifiant_patient.length()-1) != '0'&&
				 * Identifiant_patient.charAt(Identifiant_patient.length()-2) != '0') {
				 * PatDirectoryNum = PatDirectoryNum.substring(i); // Le numero de dossier Radio
				 * carestream-Jade se constitue par les derniers chiffres apres les derniers
				 * deux 0 qui se suivent (Exemple : 100500245 => 245 ou 10050544 =>50544) } else
				 * { //if(Identifiant_patient.charAt(Identifiant_patient.length()-3) == '0'&&
				 * Identifiant_patient.charAt(Identifiant_patient.length()-4) == '0') //{
				 * 
				 * //}
				 * 
				 * PatDirectoryNum = PatDirectoryNum.substring(k); // Dans le cas o� les deux
				 * derniers chiffres sont egaux � 0 (Exemple 0010500 => 10500); l'indice k
				 * contient l'avant derniere position de deux 00 qui se suivent }
				 */

				//////////////////////////////////////////////////////////////////

				File CurrentPatientDirectory_Jade = new File(PatientPathDirectory + "\\" + PatDirectoryNum + ".RVG");

				if (!PatientsDirectory.exists()) {
					if (PatientsDirectory.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
					{

						if (!CurrentPatientDirectory_Jade.exists()) {
							if (CurrentPatientDirectory_Jade.mkdir()) //// Cr�ation du repertoire Patient s'il n'existe
																		//// pas
							{
								System.out.println("Repertoire Patient Cr�e"); // cr�e un patient a nouveau

							}
						}
					} else {
						System.out.println("Erreur lors de la creation du repertoire");
					}
				} else {
					if (!CurrentPatientDirectory_Jade.exists()) {
						if (CurrentPatientDirectory_Jade.mkdir()) {
							System.out.println("Repertoire Patient Cr�e");
						} else {
							System.out.println("Erreur lors de la creation du repertoire patient");
						}
					}
				}
				PathExe = "\"" + PathExe + "\"";
				MyExe = PathExe + " -P" + CurrentPatientDirectory_Jade + " -N" + NomPatient + " -F" + PrenomPatient
						+ " -B" + Date_naissance_CSImaging + " -D" + Identifiant_patient.substring(1) + " -G" + Sexe;

			}
			/*********************
			 * Regle creation de dossier patient ODS
			 *********************/

			else if (Character.toLowerCase(Identifiant_patient.charAt(0)) == 'o') {
				PatDirectoryNum = Identifiant_patient.substring(1);
				if (PatDirectoryNum.length() < 7) {
					PatDirectoryNum = StringUtils.leftPad(PatDirectoryNum, 7, "0"); // M�thode disponible dans la
																					// biblioth�que externe
																					// "org.apache.commons.lang.StringUtils"
				}

				File CurrentPatientDirectory_ODS = new File(PatientPathDirectory + "\\" + "P" + PatDirectoryNum);

				if (!PatientsDirectory.exists()) {
					if (PatientsDirectory.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
					{

						if (!CurrentPatientDirectory_ODS.exists()) {
							if (CurrentPatientDirectory_ODS.mkdir()) //// Cr�ation du repertoire Patient s'il n'existe
																		//// pas
							{
								System.out.println("Repertoire Patient Cr�e");
							}
						}
					} else {
						System.out.println("Erreur lors de la creation du repertoire");
					}
				} else {
					if (!CurrentPatientDirectory_ODS.exists()) {
						if (CurrentPatientDirectory_ODS.mkdir()) {
							System.out.println("Repertoire Patient Cr�e");
						} else {
							System.out.println("Erreur lors de la creation du repertoire patient");
						}
					}
				}
				PathExe = "\"" + PathExe + "\"";
				MyExe = PathExe + " -P" + CurrentPatientDirectory_ODS + " -N" + NomPatient + " -F" + PrenomPatient
						+ " -B" + Date_naissance_CSImaging + " -D" + Identifiant_patient.substring(1) + " -G" + Sexe;

			} else // regle Hors ODS et Hors Jade
			{

				File CurrentPatientDirectory = new File(PatientPathDirectory + "\\" + Identifiant_patient);

				if (!PatientsDirectory.exists()) {
					if (PatientsDirectory.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
					{

						if (!CurrentPatientDirectory.exists()) {
							if (CurrentPatientDirectory.mkdir()) //// Cr�ation du repertoire Patient s'il n'existe pas
							{
								System.out.println("Repertoire Patient Cr�e");
							}
						}
					} else {
						System.out.println("Erreur lors de la creation du repertoire");
					}
				} else {
					if (!CurrentPatientDirectory.exists()) {
						if (CurrentPatientDirectory.mkdir()) {
							System.out.println("Repertoire Patient Cr�e");
						} else {
							System.out.println("Erreur lors de la creation du repertoire patient");
						}
					}
				}
				PathExe = "\"" + PathExe + "\"";
				MyExe = PathExe + " -P" + CurrentPatientDirectory + " -N" + NomPatient + " -F" + PrenomPatient + " -B"
						+ Date_naissance_CSImaging + " -D" + Identifiant_patient + " -G" + Sexe;

			}

			// System.out.println(Path);

			break;

		case "MyRay":

			String PathExe_MyRay = Path.split(";")[0];
			String Date_naissance_MyRay = Date_naissance.replace("-", ",");
            String Sex_MyRay = Sexe.substring(0, 1);
			/*String PatientPathDirectory_MyRay = Path.split(";")[1];
			File PatientsDirectory_MyRay = new File(PatientPathDirectory_MyRay);

			if (!PatientsDirectory_MyRay.exists()) {
				if (PatientsDirectory_MyRay.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
				{
					System.out.println("Repertoire Patient Cr�e");
				} else {
					System.out.println("Erreur lors de la creation du repertoire patient");
				}
			}*/
			PathExe_MyRay = "\"" + PathExe_MyRay + "\"";

			//MyExe = PathExe_MyRay + " /patid " + Identifiant_patient + " /name " + "\"" + PrenomPatient + "\""
			//		+ " /surname " + "\"" + NomPatient + "\"" + " /dateb " + Date_naissance + " /dir "
			//		+ PatientsDirectory_MyRay;
			MyExe = PathExe_MyRay +
					" /patid " + Identifiant_patient +
					" /name " + "\"" + PrenomPatient + "\"" +
					" /surname " + "\"" + NomPatient + "\"" +
					" /dateb " + Date_naissance_MyRay +
					" /sex " + Sex_MyRay;

			break;

		case "VixWin":

			String PathExe_VixWin = Path.split(";")[0];
			String PatientPathDirectory_VixWin = Path.split(";")[1];
			File PatientsDirectory_VixWin = new File(PatientPathDirectory_VixWin);
			String IDPatient_VixWin = "";

			if (!PatientsDirectory_VixWin.exists()) {
				if (PatientsDirectory_VixWin.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
				{
					System.out.println("Repertoire Patient Cr�e");
				} else {
					System.out.println("Erreur lors de la creation du repertoire patient");
				}
			}

			if (Character.toLowerCase(Identifiant_patient.charAt(0)) == 'o') {
				IDPatient_VixWin = Identifiant_patient.substring(1);
				if (IDPatient_VixWin.length() < 6) {
					IDPatient_VixWin = StringUtils.leftPad(IDPatient_VixWin, 6, "0"); // M�thode disponible dans la
																						// biblioth�que externe
																						// "org.apache.commons.lang.StringUtils"
				}
			} else if (Character.toLowerCase(Identifiant_patient.charAt(0)) == 'j') {
				IDPatient_VixWin = Identifiant_patient.substring(1);
			} else {
				IDPatient_VixWin = Identifiant_patient;
			}

			PathExe_VixWin = "\"" + PathExe_VixWin + "\"";
			MyExe = PathExe_VixWin + " -I " + IDPatient_VixWin + " -N " + "\"" + PrenomPatient + "\"" + "^" + "\""
					+ NomPatient + "\"";
			break;

		case "DBSWIN":

			File PatImport = new File("C:\\DBSWIN\\patimport.txt");
			try {
				if (!PatImport.exists())
					PatImport.createNewFile();

				// SimpleDateFormat sm_Durr = new SimpleDateFormat("ddmmyyyy");
				SimpleDateFormat sm_Durr = new SimpleDateFormat("dd-MM-yyyy");
				Date DateNaissance_Durr = sm_Durr.parse(Date_naissance);
				SimpleDateFormat sdfDestination_Durr = new SimpleDateFormat("dd.MM.yyyy");
				Date_naissance = sdfDestination_Durr.format(DateNaissance_Durr);

				FileWriter PatImportWriter = new FileWriter(PatImport, false);
				/*
				 * PatImportWriter.write(NomPatient + ";" + PrenomPatient + ";" + Date_naissance
				 * + ";" + NumSecu + ";" + ville + ";" + adresse + ";" + numero_fax + ";" +
				 * medecin + ";" + Sexe + ";" + Identifiant_patient + ";");
				 */

				PatImportWriter.write(
						NomPatient + ";" + PrenomPatient + ";" + Date_naissance + ";" + Identifiant_patient + ";");
				PatImportWriter.close();

			} catch (IOException e) {
				e.printStackTrace();
			}
			Path = "\"" + Path + "\"";
			MyExe = Path;
			break;

		case "Scanora":
			Path = "\"" + Path + "\"";
			SimpleDateFormat sm_Scanora = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Scanora = sm_Scanora.parse(Date_naissance);
			SimpleDateFormat sdfDestination_Scanora = new SimpleDateFormat("dd/MM/yyyy");
			Date_naissance = sdfDestination_Scanora.format(DateNaissance_Scanora);
			MyExe = Path + " -" + Identifiant_patient + ";\"" + PrenomPatient + "\";\"" + NomPatient + "\";;"
					+ Date_naissance + ";";

			break;
		case "Romexis":

			String PatDirectoryNum_Romexis = "";

			if (Character.toLowerCase(Identifiant_patient.charAt(0)) == 'j') // Identifiant Jade = prefix� par 'j'
			{
				PatDirectoryNum_Romexis = Identifiant_patient.substring(1);
				int j = 0;
				for (int i = 0; i < PatDirectoryNum_Romexis.length(); i++) {
					if (i < PatDirectoryNum_Romexis.length() - 1) {
						if (PatDirectoryNum_Romexis.charAt(i) == '0' && PatDirectoryNum_Romexis.charAt(i + 1) == '0') {
							j = i + 1;
							while (PatDirectoryNum_Romexis.charAt(j) == '0'
									&& j < PatDirectoryNum_Romexis.length() - 1) {
								j++;
							}
							break;
						}
					}
				}
				if (j > 0)
					PatDirectoryNum_Romexis = PatDirectoryNum_Romexis.substring(j);
			}

			else if (Character.toLowerCase(Identifiant_patient.charAt(0)) == 'o') {
				PatDirectoryNum_Romexis = Identifiant_patient.substring(1);
			}

			else {
				PatDirectoryNum_Romexis = Identifiant_patient;
			}
			Path = "\"" + Path + "\"";
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaiss = sdf.parse(Date_naissance);
			SimpleDateFormat sdfdestination = new SimpleDateFormat("dd/MM/yyyy");
			Date_naissance = sdfdestination.format(DateNaiss);
			MyExe = Path + " \"" + PatDirectoryNum_Romexis + "\"  \"" + NomPatient + "\"  \"" + PrenomPatient + "\"  \""
					+ Date_naissance + "\""; /// C:\Program Files\Planmeca\Romexis\pmbridge\Program\DxStartW.exe Chemin
												/// de l'exe
			break;

		case "CliniView":
			Path = "\"" + Path + "\"";
			SimpleDateFormat sm_Clini = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Clini = sm_Clini.parse(Date_naissance);
			SimpleDateFormat sdfDestination_Clini = new SimpleDateFormat("dd/MM/yyyy");
			Date_naissance = sdfDestination_Clini.format(DateNaissance_Clini);
			MyExe = Path + " -" + Identifiant_patient + ";\"" + PrenomPatient + "\";\"" + NomPatient + "\";;"
					+ Date_naissance + ";";

			break;

		case "QuickVision":
			Path = "\"" + Path + "\"";
			SimpleDateFormat sm_Quick = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Quick = sm_Quick.parse(Date_naissance);
			SimpleDateFormat sdfDestination_Quick = new SimpleDateFormat("dd-MM-yyyy");
			Date_naissance = sdfDestination_Quick.format(DateNaissance_Quick);

			MyExe = Path + " C /ALINK";

			break;

		case "Sirona": // DCBL : en attente d'obtenir les champs de la part du service technique
			String sidexisApp = Path.split(";")[0];
			String sdxFile = Path.split(";")[1];
			String stationName = Path.split(";")[2];
			sidexisApp = "\"" + sidexisApp + "\"";
			SimpleDateFormat sm_Sirona = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Sirona = sm_Sirona.parse(Date_naissance);
			SimpleDateFormat sdfDestination_Sirona = new SimpleDateFormat("dd.MM.yyyy");
			Date_naissance = sdfDestination_Sirona.format(DateNaissance_Sirona);

			//MyExe = Path + " -" + Identifiant_patient + ";\"" + PrenomPatient + "\";\"" + NomPatient + "\";;"
			//		+ Date_naissance + ";";
			MyExe = sidexisApp;
			try (DataOutputStream sdxOut = new DataOutputStream(new FileOutputStream(sdxFile)))
			{
	            String doctor = " ";
	            String station = stationName;
	            String vdImaging = "VDImaging";
	            String sender = "\\\\" + station + "\\" + vdImaging;
	            String receiver = "\\\\*\\SIDEXIS";
	            String requestDate = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
	            String requestTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
	            String image = "0";
	            String sex = Sexe.substring(0, 1);
	            int l = 1 + 1 // length
	            		+ 1 + 1 // token
	            		+ NomPatient.length() + 1 // first name
	            		+ PrenomPatient.length() + 1 // first name
	            		+ Date_naissance.length() + 1 // date of birth
	            		+ Identifiant_patient.length() + 1 // card no
	            		+ sex.length() + 1 // sex
	            		+ doctor.length() + 1 // doctor
	            		+ sender.length() + 1 // sender
	            		+ receiver.length() + 1 // receiver
	            		+ 1 + 1 // end of token
	            		;
	            char token = 'N';
	            char eos = '\0';

				sdxOut.write(l);
				sdxOut.write(eos);
				sdxOut.write(token);
				sdxOut.write(eos);
				sdxOut.write(NomPatient.getBytes());
				sdxOut.write(eos);
				sdxOut.write(PrenomPatient.getBytes());
				sdxOut.write(eos);
				sdxOut.write(Date_naissance.getBytes());
				sdxOut.write(eos);
				sdxOut.write(Identifiant_patient.getBytes());
				sdxOut.write(eos);
				sdxOut.write(sex.getBytes());
				sdxOut.write(eos);
				sdxOut.write(doctor.getBytes());
				sdxOut.write(eos);
				sdxOut.write(sender.getBytes());
				sdxOut.write(eos);
				sdxOut.write(receiver.getBytes());
				sdxOut.write(eos);
				sdxOut.write('\r');
				sdxOut.write('\n');

	            l = 1 + 1 // length
            		+ 1 + 1 // token
            		+ NomPatient.length() + 1 // first name
            		+ PrenomPatient.length() + 1 // first name
            		+ Date_naissance.length() + 1 // date of birth
            		+ Identifiant_patient.length() + 1 // card no
            		+ station.length() + 1 // station
            		+ requestDate.length() + 1 // requestDate
            		+ requestTime.length() + 1 // requestTime
            		+ sender.length() + 1 // sender
            		+ receiver.length() + 1 // receiver
            		+ image.length() + 1 // image
            		+ 1 + 1 // end of token
            		;
                token = 'A';
                sdxOut.write(l);
				sdxOut.write(eos);
                sdxOut.write(token);
                sdxOut.write(eos);
                sdxOut.write(NomPatient.getBytes());
                sdxOut.write(eos);
                sdxOut.write(PrenomPatient.getBytes());
                sdxOut.write(eos);
                sdxOut.write(Date_naissance.getBytes());
                sdxOut.write(eos);
                sdxOut.write(Identifiant_patient.getBytes());
                sdxOut.write(eos);
                sdxOut.write(station.getBytes());
                sdxOut.write(eos);
                sdxOut.write(requestDate.getBytes());
                sdxOut.write(eos);
                sdxOut.write(requestTime.getBytes());
                sdxOut.write(eos);
                sdxOut.write(sender.getBytes());
                sdxOut.write(eos);
                sdxOut.write(receiver.getBytes());
                sdxOut.write(eos);
                sdxOut.write(image.getBytes());
                sdxOut.write(eos);
                sdxOut.write('\r');
                sdxOut.write('\n');

                sdxOut.close();
			} catch (Exception e) {	}

			break;

		case "Vatech":
			File file1 = new File("C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientcreate.xml");
			file1.delete();
			File file2 = new File("C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientsearch.xml");
			file2.delete();
			File file3 = new File("C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientresult.xml");
			file3.delete();
			File file4 = new File("C:\\Program Files (x86)\\VATECH\\EzDent-i\\resultcreation.xml");
			file4.delete();
			
			Path = "\"" + Path + "\"";

			SimpleDateFormat sm_Vatech = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Vatech = sm_Vatech.parse(Date_naissance);
			SimpleDateFormat sdfDestination_Vatech = new SimpleDateFormat("MM-dd-yyyy");
			Date_naissance = sdfDestination_Vatech.format(DateNaissance_Vatech);
			
			
			String fichierxmlin = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + "<Request>\n" + "<SearchPatient>\n"
					+ "<Search ChartNo=\"" + Identifiant_patient + "\"/>\n" + "</SearchPatient>\n" + "</Request>\n";
			
			
			try (Writer writer = new BufferedWriter(new OutputStreamWriter(
		              new FileOutputStream("C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientsearch.xml"), "utf-8"))) {
				   writer.write(fichierxmlin);
			} catch (UnsupportedEncodingException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}

			Runtime Run = Runtime.getRuntime();

			try {
				Process p1 = Run.exec("\"C:\\Program Files (x86)\\VATECH\\EzDent-i\\Bin\\VTEzBridge32.exe\" /in:\"C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientsearch.xml\" /out:\"C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientresult.xml\"");
			    p1.waitFor();

			} catch (IOException e1) {
				e1.printStackTrace();
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			try(BufferedReader br = new BufferedReader(new FileReader("C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientresult.xml"))) {
			    StringBuilder sb = new StringBuilder();
			    String crtline = br.readLine();

			    while (crtline != null) {
			        sb.append(crtline);
			        sb.append(System.lineSeparator());
			        crtline = br.readLine();
			    }
			    String everything = sb.toString();
			    

				if(everything.contains("No such data in DB")) {
					
					String newpatientxml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
							"<Request>\n" + 
							"<CreatePatient>\n" + 
							"<Patient ChartNo=\""+Identifiant_patient+"\" FirstName=\""+PrenomPatient+"\" LastName=\""+NomPatient+"\" Gender=\""+Sexe+"\"\n" + 
							"Birthdate=\""+Date_naissance+"\"/>\n" + 
							"</CreatePatient>\n" + 
							"</Request>";
					
					try (Writer writer = new BufferedWriter(new OutputStreamWriter(
				              new FileOutputStream("C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientcreate.xml"), "utf-8"))) {
						   writer.write(newpatientxml);
					} catch (UnsupportedEncodingException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (FileNotFoundException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					} catch (IOException e1) {
						// TODO Auto-generated catch block
						e1.printStackTrace();
					}
					
					
					MyExe = Path + " /in:\"C:\\Program Files (x86)\\VATECH\\EzDent-i\\crtpatientcreate.xml\" /out:\"C:\\Program Files (x86)\\VATECH\\EzDent-i\\resultcreation.xml\""+ " /run:" + Identifiant_patient;
				}
				else {
					MyExe = Path + " /run:" + Identifiant_patient;
				}
			    
			} catch (FileNotFoundException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (IOException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			break;
		case "Sidexis":
			
			break;

		default:
			System.exit(0);

		}
		try {
			Runtime Run = Runtime.getRuntime();

			Run.exec(MyExe);

			/*
			 * if(Marque.equals("QuickVision")) { String Cmd =
			 * "/PA:<"+Identifiant_patient+">,"+NomPatient+","+PrenomPatient+","+NumSecu+","
			 * +Date_naissance;
			 * 
			 * 
			 * User32 user32;
			 * 
			 * HWND m_hWndLink = user32.FindWindow("MjLinkWndClass", null); LPARAM param =
			 * Cmd.getClass().cast();
			 * 
			 * user32.PostMessage(m_hWndLink, 0x000C, null, param);
			 * 
			 * }
			 */
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
}
