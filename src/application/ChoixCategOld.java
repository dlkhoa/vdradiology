package application;
import java.io.File;
import java.util.HashMap;
import java.util.Map;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

public class ChoixCategOld extends Application
{
	public void start(Stage primaryStage) 
	{
		GridPane Grid_Choice = new GridPane();
		
		Image Image_Capteur = new Image("file:bin/application/Images/Capteur_Dentaire.jpg");				
		Image Image_Pano = new Image("file:bin/application/Images/pano_dentaire.jpg");
		Image Image_Camera = new Image("file:bin/application/Images/Camera_Dentaire.jpg");		
		ImageView Capteur_ImageView = new ImageView(Image_Capteur);
		ImageView Pano_ImageView = new ImageView(Image_Pano);
		ImageView Camera_ImageView = new ImageView(Image_Camera);
		
		Capteur_ImageView.setFitHeight(200);
		Capteur_ImageView.setFitWidth(200);
		Pano_ImageView.setFitHeight(200);
		Pano_ImageView.setFitWidth(200);
		Camera_ImageView.setFitHeight(200);
		Camera_ImageView.setFitWidth(200);
		Button Btn_Capteur = new Button();
		Btn_Capteur.setGraphic(Capteur_ImageView);
		Btn_Capteur.setMinSize(200, 200);
		Button Btn_Pano = new Button();
		Btn_Pano.setGraphic(Pano_ImageView);
		Btn_Pano.setMinSize(200, 200);		
		Button Btn_Camera = new Button();
		Btn_Camera.setGraphic(Camera_ImageView);
		Btn_Camera.setMinSize(200,200);
		
 		
		Grid_Choice.setStyle("-fx-background-color: AliceBlue;");
		

		//////////////////////Grid Param////////////////
		
		GridPane Grid_Param = new GridPane();
		String Categ = new String();
		
		Button Btn_Previous = new Button("Precedent");
		Btn_Previous.setPrefSize(100, 40);
		Button Browse = new Button("Parcourir...");
		Browse.setPrefSize(100, 10);
		Button Btn_executer = new Button("Executer");
		//List <Object> List_Exe = new List<Object>;
		 Map<String,String> List_Exe_Map = new HashMap<String,String>();
		
		Btn_executer.setAlignment(Pos.BASELINE_CENTER);
		Btn_executer.setPrefSize(100, 20);
		Label label = new Label(" Aplication : ");
		//Label label1 = new Label(Categ);			
		FileChooser Chooser = new FileChooser();
		TextField textField = new TextField ();
		textField.setPrefSize(400, 10);
		
		//ListView<String> listMarque = new ListView<String>();
		ObservableList<String> listCapteur =FXCollections.observableArrayList ("Kodak", "Visiodent", "Cirona","PlanMeca","Durr","Digora");
		final ComboBox<String> comboBox = new ComboBox<String>(listCapteur);
		comboBox.setPrefSize(400,10);
		comboBox.getSelectionModel().selectFirst();
		
		Browse.setOnAction(actionEvent -> 
		{ 				
			File file = Chooser.showOpenDialog(primaryStage.getScene().getWindow());   
		    if (file != null) 
		    {
		    	textField.setText(file.getPath());			         
		    } 
		});
		
		
		Btn_executer.setOnAction(actionEvent -> 
		{ 			
		   List_Exe_Map.put(comboBox.getSelectionModel().getSelectedItem(),textField.getText());		   		   
		   System.out.println(List_Exe_Map);		   
		});
		
		Grid_Param.add(Browse,2 , 3);
		Grid_Param.add(label, 0, 2);
		Grid_Param.add(textField, 1,3);		
		Grid_Param.add(Btn_executer, 2, 10);
		Grid_Param.add(Btn_Previous, 2, 15);
		Grid_Param.setAlignment(Pos.BASELINE_CENTER);
		Grid_Param.add(comboBox, 1, 2);

		Grid_Param.setHgap(6);
		Grid_Param.setVgap(6);
		Scene scene_Param = new Scene(Grid_Param,400,400);
		scene_Param.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setTitle("Parametrage "+Categ);
		
		primaryStage.setWidth(650);
		primaryStage.setHeight(400);
					
	/////////////////////////////////////////////////////grid param/////////////////////////////	 				
								
		Btn_Capteur.setOnAction(actionEvent -> 
		{	
			String MyChoice = new String("Capteur");			
			primaryStage.setScene(scene_Param);
			primaryStage.setWidth(650);
			primaryStage.setHeight(400);
			System.out.println(MyChoice);					 
		});
		
		Btn_Pano.setOnAction(actionEvent -> 
		{	
			String MyChoice = new String("Panoramique");			
			primaryStage.setScene(scene_Param);
			primaryStage.setWidth(650);
			primaryStage.setHeight(400);			
			System.out.println(MyChoice);
			
		});
		
		Btn_Camera.setOnAction(actionEvent -> 
		{	
			String MyChoice = new String("Camera");			
			primaryStage.setScene(scene_Param);
			primaryStage.setWidth(650);
			primaryStage.setHeight(400);
			System.out.println(MyChoice);
		});
				
		Grid_Choice.add(Btn_Capteur,2 , 3);
		Grid_Choice.add(Btn_Pano,2 , 4);
		Grid_Choice.add(Btn_Camera,3 , 3);						
		
		
		Grid_Choice.setHgap(7);
		Grid_Choice.setVgap(7);
		Scene scene = new Scene(Grid_Choice,400,400);
		
/////////////////////////////////////////////////Grid_Param///////////////////////////////
		Btn_Previous.setOnAction(actionEvent -> 
		{ 				
			primaryStage.setScene(scene);
			primaryStage.setWidth(500);
			primaryStage.setHeight(500);
				
		});
/////////////////////////////////////////////////Grid_Param///////////////////////////////		
		scene.getStylesheets().add(getClass().getResource("application.css").toExternalForm());
		primaryStage.setTitle("Choix Categorie ");
		primaryStage.setScene(scene);
		primaryStage.setWidth(500);
		primaryStage.setHeight(500);
		primaryStage.show();
		
	}
	
	public static void main(String[] args) 
	{
		launch(args);

	}

}
