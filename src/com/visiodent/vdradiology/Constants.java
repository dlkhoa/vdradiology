package com.visiodent.vdradiology;

import java.io.File;

public class Constants
{
	public static final String SettingFile =
			System.getenv("public") + 
			File.separator +
			"SettingFileRelaisImagerie.txt";
	public static final String ResourceLocation = "/resources/";
	
	public static final String TitleCategory = "Indiquer l'emplacement de l'application imagerie";
	public static final String TitleParam = "Parametrage - ";
	public static final String TitleInformation = "Information";

	public static final int Button_Width = 250;
	public static final int Button_Height = 40;
	public static final int SceneMain_Gap = 10;

	public static final int CategoryButton_FitWidth = 200;
	public static final int CategoryButton_FitHeight = 200;
	public static final int SceneCategory_Width = 500;
	public static final int SceneCategory_Height = 550;
	public static final int SceneCategory_Gap = 10;
	public static final int SceneParam_Width = 850;
	public static final int SceneParam_Height = 400;
	public static final int SceneParam_Gap = 10;
	public static final int ParamButton_Width = 120;
	public static final int ParamButton_Height = 20;
	public static final int ParamText_Width = 400;
	public static final int ParamText_Height = 10;
	public static final int ParamCombo_Width = 400;
	public static final int ParamCombo_Height = 10;

	public static final String Resource_Css = "application.css";
	public static final String Resource_Capteur = "capteur.jpg";
	public static final String Resource_Camera = "camera.jpg";
	public static final String Resource_Panoramique = "panoramique.jpg";
	public static final String Resource_Logo1 = "logo_visiodent1.jpg";
	public static final String Resource_Logo2 = "logo_visiodent2.jpg";

	public static final String WebParam_Categorie = "CATEGORIE";
	public static final String WebParam_NomPatient = "NOMPATIENT";
	public static final String WebParam_PrenomPatient = "PRENOMPATIENT";
	public static final String WebParam_DateNaissance = "DATE_NAISSANCE";
	public static final String WebParam_IdentifiantPatient = "IDENTIFIANT_PATIENT";
	public static final String WebParam_SexePatient = "SEXE";

	public static final String Categorie_Capteur = "Capteur";
	public static final String Categorie_Camera = "Camera";
	public static final String Categorie_Panoramique = "Panoramique";

	public static final String Imagerie_CSImaging = "CSImaging";
	public static final String Imagerie_RSVImaging = "RSVImaging";
	public static final String Imagerie_Romexis = "Romexis";
	public static final String Imagerie_DBSWIN = "DBSWIN";
	public static final String Imagerie_Scanora = "Scanora";
	public static final String Imagerie_CliniView = "CliniView";
	public static final String Imagerie_MyRay = "MyRay";
	public static final String Imagerie_VixWin = "VixWin";
	public static final String Imagerie_Sirona = "Sirona";
	public static final String Imagerie_Vatech = "Vatech";
	public static final String Imagerie_QuickVision = "QuickVision";
	public static final String Imagerie_DFW = "DFW";
	public static final String Imagerie_Sopro = "Sopro";
}
