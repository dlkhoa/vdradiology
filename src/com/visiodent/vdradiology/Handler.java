package com.visiodent.vdradiology;

import java.text.ParseException;
import java.util.Map;

import com.visiodent.vdradiology.imagerie.ApplicationFactory;
import com.visiodent.vdradiology.imagerie.ImagerieApplication;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Handler extends Application
{
	private WebParameters webParameters = null;
	private Map<String, ImagerieApplication> imagerieApplications = null;

	public static void main(String[] args)
	{
        ApplicationFactory.loadApplicationList();
		launch(args);
	}

	@Override
	public void start(Stage primaryStage) throws ParseException
	{
		initWebParameters();
		initApplications();

		GridPane gridRelaisImagerie = new GridPane();

		////////////// Cas o� le praticien dispose de plusieurs Systeme d'imagerie
		////////////// /////////////////////////////////
		int k = 0;
		for (Map.Entry<String, ImagerieApplication> mapEntry : imagerieApplications.entrySet())
		{
			String imagerieName = mapEntry.getKey();
			Button btnImagerie = createImagerieButton(imagerieName);
			btnImagerie.setOnAction(actionEvent ->
			{
				imagerieHandle(imagerieName);
			});
			gridRelaisImagerie.add(btnImagerie, 8, k + 3);
			k++;
		}

		int width = Constants.Button_Width + 18 * Constants.SceneMain_Gap;
		int height = k * (Constants.Button_Height + 2 * Constants.SceneMain_Gap) + 8 * Constants.SceneMain_Gap;

		gridRelaisImagerie.setHgap(Constants.SceneMain_Gap);
		gridRelaisImagerie.setVgap(Constants.SceneMain_Gap);
		Scene scene = new Scene(gridRelaisImagerie, width, height);
		scene.getStylesheets().add(Utils.getResourceString(getClass(), Constants.Resource_Css));

		primaryStage.setWidth(width);
		primaryStage.setHeight(height);
		primaryStage.setTitle("Menu Syst�mes imagerie");
		primaryStage.setScene(scene);

		if (imagerieApplications.size() > 1)
		{
			primaryStage.show();
		}
		else
		{
			if (imagerieApplications.size() == 1)
			{
				String imagerieName = (String)imagerieApplications.keySet().toArray()[0];
				imagerieHandle(imagerieName);
			}
			System.exit(0);
		}

	}

	private void imagerieHandle(String imagerieName)
	{
		ImagerieApplication imagerieApplication = imagerieApplications.get(imagerieName);
		imagerieApplication.handle(webParameters);
	}

	private void initWebParameters()
	{
		if (webParameters == null)
		{
			if (getParameters() != null && getParameters().getNamed() != null)
			{
				webParameters = Utils.getWebParameters(getParameters().getUnnamed().toString());
			}
		}
	}

	private void initApplications()
	{
		if (imagerieApplications == null)
		{
			imagerieApplications = ApplicationFactory.getApplicationList(true);
		}
	}

	private Button createImagerieButton(String imagerieName)
	{
		Button btnImagerie = new Button(imagerieName);
		btnImagerie.setMinSize(Constants.Button_Width, Constants.Button_Height);

		return btnImagerie;
	}
}
