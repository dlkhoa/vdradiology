package com.visiodent.vdradiology;

import java.io.File;
import java.io.InputStream;
import java.net.URLDecoder;

import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;

public class Utils
{
	public static WebParameters getWebParameters(String parametersString)
	{
		WebParameters webParameters = new WebParameters();
		String decodedParametersString = parametersString;
		try 
		{
			decodedParametersString = URLDecoder.decode(parametersString, "UTF-8" );
		} catch (Exception e) {}
		String[] lines = decodedParametersString.split("//");
		if (lines != null && lines.length > 1) {
			String[] nameValues = lines[1].split(";");
			if (nameValues != null && nameValues.length > 0) {
				for (int i = 0; i < nameValues.length; i++) {
					String[] nameValue = nameValues[i].split("=");
					if (nameValue != null && nameValue.length > 1) {
						String name = nameValue[0];
						String value = nameValue[1];
						switch (name.toUpperCase())
						{
						case Constants.WebParam_Categorie:
							webParameters.Categorie = value;
							break;
						case Constants.WebParam_NomPatient:
							webParameters.NomPatient = value;
							break;
						case Constants.WebParam_PrenomPatient:
							webParameters.PrenomPatient = value;
							break;
						case Constants.WebParam_DateNaissance:
							webParameters.DateNaissance = value;
							break;
						case Constants.WebParam_IdentifiantPatient:
							webParameters.IdentifiantPatient = value;
							break;
						case Constants.WebParam_SexePatient:
							webParameters.SexePatient = value.replace("]", "");
							break;
						default:
							break;
						}
					}
				}
			}
		}
		
		return webParameters;
	}

	public static void showDialog(String title, String text)
	{
		Alert alert = new Alert(AlertType.INFORMATION);
		alert.setTitle(title);
		alert.setHeaderText(null);
		alert.setContentText(text);
		alert.showAndWait();		   						   						
	}

	public static String getResourceString(Class<?> objectClass, String resourceName)
	{
        return objectClass.getResource(Constants.ResourceLocation + resourceName).toExternalForm();
	}

	public static InputStream getResourceStream(Class<?> objectClass, String resourceName)
	{
        return objectClass.getResourceAsStream(Constants.ResourceLocation + resourceName);
	}

	public static String getDirectoryPath(String filePath, String search)
	{
        return filePath.substring(0, filePath.toLowerCase().lastIndexOf(search.toLowerCase()));
	}

	public static String getDirectoryPath(String filePath)
	{
        return getDirectoryPath(filePath, File.separator);
	}
	
}
