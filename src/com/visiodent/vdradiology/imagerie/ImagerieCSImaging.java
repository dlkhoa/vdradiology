package com.visiodent.vdradiology.imagerie;

import java.io.File;

import org.apache.commons.lang.StringUtils;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieCSImaging extends ImagerieApplication
{
	public ImagerieCSImaging()
	{
		name = Constants.Imagerie_CSImaging;
	}

	@Override
	public void prepare(WebParameters webParameters)
	{
		//String Path = getParameter1();
		String PatDirectoryNum = "";
		String PathExe = getParameter1();
		String PatientPathDirectory = getParameter2();
		File PatientsDirectory = new File(PatientPathDirectory);
		String Date_naissance_CSImaging = webParameters.DateNaissance.replace("-", "/");

		if (Character.toLowerCase(webParameters.IdentifiantPatient.charAt(0)) == 'j') // teste si le premier caractere est un "j"
																			// => cela signifie qu'il s'agit d'un
																			// identifiant Jade
		{
			PatDirectoryNum = webParameters.IdentifiantPatient.substring(1); // PatDirectoryNum Contient le IDPatient sans la
																// lettre "J"

			/*********************
			 * Regle creation de dossier patient jade
			 *********************/

			int i = 0;

			while (i < PatDirectoryNum.length() && PatDirectoryNum.charAt(i) == '0') {
				i++;
			}

			if (PatDirectoryNum.charAt(0) == '0') {
				PatDirectoryNum = PatDirectoryNum.substring(i);
			}

			////////////////////////////////////////////////////////////////////////

			// � garder en cas de changement de regle de la part de

			// int k = 0;

			/*
			 * for(int j = 1;j<Identifiant_patient.length();j++) {
			 * //if((Identifiant_patient.charAt(Identifiant_patient.length()-3)
			 * ==Identifiant_patient.charAt(j) && Identifiant_patient.charAt(j) == '0') ||
			 * (Identifiant_patient.charAt(Identifiant_patient.length()-4) ==
			 * Identifiant_patient.charAt(j) && Identifiant_patient.charAt(j)== '0')) //{
			 * k=i; //} if(Identifiant_patient.charAt(j) == '0' &&
			 * Identifiant_patient.charAt(j-1) == '0') { i=j; //enregistre la position du j
			 * } }
			 * 
			 * if(Identifiant_patient.charAt(Identifiant_patient.length()-1) != '0'&&
			 * Identifiant_patient.charAt(Identifiant_patient.length()-2) != '0') {
			 * PatDirectoryNum = PatDirectoryNum.substring(i); // Le numero de dossier Radio
			 * carestream-Jade se constitue par les derniers chiffres apres les derniers
			 * deux 0 qui se suivent (Exemple : 100500245 => 245 ou 10050544 =>50544) } else
			 * { //if(Identifiant_patient.charAt(Identifiant_patient.length()-3) == '0'&&
			 * Identifiant_patient.charAt(Identifiant_patient.length()-4) == '0') //{
			 * 
			 * //}
			 * 
			 * PatDirectoryNum = PatDirectoryNum.substring(k); // Dans le cas o� les deux
			 * derniers chiffres sont egaux � 0 (Exemple 0010500 => 10500); l'indice k
			 * contient l'avant derniere position de deux 00 qui se suivent }
			 */

			//////////////////////////////////////////////////////////////////

			File CurrentPatientDirectory_Jade = new File(PatientPathDirectory + "\\" + PatDirectoryNum + ".RVG");

			if (!PatientsDirectory.exists()) {
				if (PatientsDirectory.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
				{

					if (!CurrentPatientDirectory_Jade.exists()) {
						if (CurrentPatientDirectory_Jade.mkdir()) //// Cr�ation du repertoire Patient s'il n'existe
																	//// pas
						{
							System.out.println("Repertoire Patient Cr�e"); // cr�e un patient a nouveau

						}
					}
				} else {
					System.out.println("Erreur lors de la creation du repertoire");
				}
			} else {
				if (!CurrentPatientDirectory_Jade.exists()) {
					if (CurrentPatientDirectory_Jade.mkdir()) {
						System.out.println("Repertoire Patient Cr�e");
					} else {
						System.out.println("Erreur lors de la creation du repertoire patient");
					}
				}
			}
			PathExe = "\"" + PathExe + "\"";
			this.executable = PathExe + " -P" + CurrentPatientDirectory_Jade + " -N" + webParameters.NomPatient + " -F" + webParameters.PrenomPatient
					+ " -B" + Date_naissance_CSImaging + " -D" + webParameters.IdentifiantPatient.substring(1) + " -G" + webParameters.SexePatient;

		}
		/*********************
		 * Regle creation de dossier patient ODS
		 *********************/

		else if (Character.toLowerCase(webParameters.IdentifiantPatient.charAt(0)) == 'o') {
			PatDirectoryNum = webParameters.IdentifiantPatient.substring(1);
			if (PatDirectoryNum.length() < 7) {
				PatDirectoryNum = StringUtils.leftPad(PatDirectoryNum, 7, "0"); // M�thode disponible dans la
																				// biblioth�que externe
																				// "org.apache.commons.lang.StringUtils"
			}

			File CurrentPatientDirectory_ODS = new File(PatientPathDirectory + "\\" + "P" + PatDirectoryNum);

			if (!PatientsDirectory.exists()) {
				if (PatientsDirectory.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
				{

					if (!CurrentPatientDirectory_ODS.exists()) {
						if (CurrentPatientDirectory_ODS.mkdir()) //// Cr�ation du repertoire Patient s'il n'existe
																	//// pas
						{
							System.out.println("Repertoire Patient Cr�e");
						}
					}
				} else {
					System.out.println("Erreur lors de la creation du repertoire");
				}
			} else {
				if (!CurrentPatientDirectory_ODS.exists()) {
					if (CurrentPatientDirectory_ODS.mkdir()) {
						System.out.println("Repertoire Patient Cr�e");
					} else {
						System.out.println("Erreur lors de la creation du repertoire patient");
					}
				}
			}
			PathExe = "\"" + PathExe + "\"";
			this.executable = PathExe + " -P" + CurrentPatientDirectory_ODS + " -N" + webParameters.NomPatient + " -F" + webParameters.PrenomPatient
					+ " -B" + Date_naissance_CSImaging + " -D" + webParameters.IdentifiantPatient.substring(1) + " -G" + webParameters.SexePatient;

		} else // regle Hors ODS et Hors Jade
		{

			File CurrentPatientDirectory = new File(PatientPathDirectory + "\\" + webParameters.IdentifiantPatient);

			if (!PatientsDirectory.exists()) {
				if (PatientsDirectory.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
				{

					if (!CurrentPatientDirectory.exists()) {
						if (CurrentPatientDirectory.mkdir()) //// Cr�ation du repertoire Patient s'il n'existe pas
						{
							System.out.println("Repertoire Patient Cr�e");
						}
					}
				} else {
					System.out.println("Erreur lors de la creation du repertoire");
				}
			} else {
				if (!CurrentPatientDirectory.exists()) {
					if (CurrentPatientDirectory.mkdir()) {
						System.out.println("Repertoire Patient Cr�e");
					} else {
						System.out.println("Erreur lors de la creation du repertoire patient");
					}
				}
			}
			PathExe = "\"" + PathExe + "\"";
			this.executable = PathExe + " -P" + CurrentPatientDirectory + " -N" + webParameters.NomPatient + " -F" + webParameters.PrenomPatient + " -B"
					+ Date_naissance_CSImaging + " -D" + webParameters.IdentifiantPatient + " -G" + webParameters.SexePatient;

		}

		// System.out.println(Path);
	}

	@Override
	public boolean visibleParameter2()
	{
		return true;			   								   					
	}
}
