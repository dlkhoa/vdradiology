package com.visiodent.vdradiology.imagerie;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieQuickVision extends ImagerieApplication
{
	public ImagerieQuickVision()
	{
		name = Constants.Imagerie_QuickVision;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		String Path = "\"" + getParameter1() + "\"";
		this.executable = Path + " C /ALINK";
	}
}
