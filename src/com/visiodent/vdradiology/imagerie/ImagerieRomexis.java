package com.visiodent.vdradiology.imagerie;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieRomexis extends ImagerieApplication
{
	public ImagerieRomexis()
	{
		name = Constants.Imagerie_Romexis;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		String PatDirectoryNum_Romexis = "";

		if (Character.toLowerCase(webParameters.IdentifiantPatient.charAt(0)) == 'j') // Identifiant Jade = prefix� par 'j'
		{
			PatDirectoryNum_Romexis = webParameters.IdentifiantPatient.substring(1);
			int j = 0;
			for (int i = 0; i < PatDirectoryNum_Romexis.length(); i++) {
				if (i < PatDirectoryNum_Romexis.length() - 1) {
					if (PatDirectoryNum_Romexis.charAt(i) == '0' && PatDirectoryNum_Romexis.charAt(i + 1) == '0') {
						j = i + 1;
						while (PatDirectoryNum_Romexis.charAt(j) == '0'
								&& j < PatDirectoryNum_Romexis.length() - 1) {
							j++;
						}
						break;
					}
				}
			}
			if (j > 0)
				PatDirectoryNum_Romexis = PatDirectoryNum_Romexis.substring(j);
		}

		else if (Character.toLowerCase(webParameters.IdentifiantPatient.charAt(0)) == 'o') {
			PatDirectoryNum_Romexis = webParameters.IdentifiantPatient.substring(1);
		}

		else {
			PatDirectoryNum_Romexis = webParameters.IdentifiantPatient;
		}
		try
		{
			String Path = "\"" + getParameter1() + "\"";
			SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaiss = sdf.parse(webParameters.DateNaissance);
			SimpleDateFormat sdfdestination = new SimpleDateFormat("dd/MM/yyyy");
			String DateNaissance = sdfdestination.format(DateNaiss);
			this.executable = Path + " \"" + PatDirectoryNum_Romexis + "\"  \"" + webParameters.NomPatient + "\"  \"" + webParameters.PrenomPatient + "\"  \""
					+ DateNaissance + "\""; /// C:\Program Files\Planmeca\Romexis\pmbridge\Program\DxStartW.exe Chemin
											/// de l'exe
		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
