package com.visiodent.vdradiology.imagerie;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

import javafx.stage.FileChooser;
import javafx.stage.Window;

public class ImagerieSirona extends ImagerieApplication
{
	public ImagerieSirona()
	{
		name = Constants.Imagerie_Sirona;
	}

	@Override
	public void prepare(WebParameters webParameters)
	{
		try
		{
			String sidexisApp = getParameter1();
			String sdxFile = getParameter2();
			String stationName = getParameter3();
			sidexisApp = "\"" + sidexisApp + "\"";
			SimpleDateFormat sm_Sirona = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Sirona = sm_Sirona.parse(webParameters.DateNaissance);
			SimpleDateFormat sdfDestination_Sirona = new SimpleDateFormat("dd.MM.yyyy");
			String DateNaissance = sdfDestination_Sirona.format(DateNaissance_Sirona);

			DataOutputStream sdxOut = new DataOutputStream(new FileOutputStream(sdxFile));
            String doctor = " ";
            String station = stationName;
            String vdImaging = "VDImaging";
            String sender = "\\\\" + station + "\\" + vdImaging;
            String receiver = "\\\\*\\SIDEXIS";
            String requestDate = new SimpleDateFormat("dd.MM.yyyy").format(new Date());
            String requestTime = new SimpleDateFormat("HH:mm:ss").format(new Date());
            String image = "0";
            String sex = webParameters.SexePatient.substring(0, 1);
            int l = 1 + 1 // length
            		+ 1 + 1 // token
            		+ webParameters.NomPatient.length() + 1 // first name
            		+ webParameters.PrenomPatient.length() + 1 // first name
            		+ DateNaissance.length() + 1 // date of birth
            		+ webParameters.IdentifiantPatient.length() + 1 // card no
            		+ sex.length() + 1 // sex
            		+ doctor.length() + 1 // doctor
            		+ sender.length() + 1 // sender
            		+ receiver.length() + 1 // receiver
            		+ 1 + 1 // end of token
            		;
            char token = 'N';
            char eos = '\0';

			sdxOut.write(l);
			sdxOut.write(eos);
			sdxOut.write(token);
			sdxOut.write(eos);
			sdxOut.write(webParameters.NomPatient.getBytes());
			sdxOut.write(eos);
			sdxOut.write(webParameters.PrenomPatient.getBytes());
			sdxOut.write(eos);
			sdxOut.write(DateNaissance.getBytes());
			sdxOut.write(eos);
			sdxOut.write(webParameters.IdentifiantPatient.getBytes());
			sdxOut.write(eos);
			sdxOut.write(sex.getBytes());
			sdxOut.write(eos);
			sdxOut.write(doctor.getBytes());
			sdxOut.write(eos);
			sdxOut.write(sender.getBytes());
			sdxOut.write(eos);
			sdxOut.write(receiver.getBytes());
			sdxOut.write(eos);
			sdxOut.write('\r');
			sdxOut.write('\n');

            l = 1 + 1 // length
        		+ 1 + 1 // token
        		+ webParameters.NomPatient.length() + 1 // first name
        		+ webParameters.PrenomPatient.length() + 1 // first name
        		+ DateNaissance.length() + 1 // date of birth
        		+ webParameters.IdentifiantPatient.length() + 1 // card no
        		+ station.length() + 1 // station
        		+ requestDate.length() + 1 // requestDate
        		+ requestTime.length() + 1 // requestTime
        		+ sender.length() + 1 // sender
        		+ receiver.length() + 1 // receiver
        		+ image.length() + 1 // image
        		+ 1 + 1 // end of token
        		;
            token = 'A';
            sdxOut.write(l);
			sdxOut.write(eos);
            sdxOut.write(token);
            sdxOut.write(eos);
            sdxOut.write(webParameters.NomPatient.getBytes());
            sdxOut.write(eos);
            sdxOut.write(webParameters.PrenomPatient.getBytes());
            sdxOut.write(eos);
            sdxOut.write(DateNaissance.getBytes());
            sdxOut.write(eos);
            sdxOut.write(webParameters.IdentifiantPatient.getBytes());
            sdxOut.write(eos);
            sdxOut.write(station.getBytes());
            sdxOut.write(eos);
            sdxOut.write(requestDate.getBytes());
            sdxOut.write(eos);
            sdxOut.write(requestTime.getBytes());
            sdxOut.write(eos);
            sdxOut.write(sender.getBytes());
            sdxOut.write(eos);
            sdxOut.write(receiver.getBytes());
            sdxOut.write(eos);
            sdxOut.write(image.getBytes());
            sdxOut.write(eos);
            sdxOut.write('\r');
            sdxOut.write('\n');

            sdxOut.close();

            this.executable = sidexisApp;
		} catch (Exception e) {	}
	}

	@Override
	public boolean visibleParameter2()
	{
		return true;			   								   					
	}

	@Override
	public boolean visibleParameter3()
	{
		return true;
	}

	@Override
	public String textParameter2()
	{
		return "Fichier mailbox (SLIDA)";
	}

	@Override
	public String defineParameter2(Window parentWindow)
	{
		FileChooser chooser = new FileChooser();
		chooser.setTitle(textParameter2());
		File file = chooser.showOpenDialog(parentWindow);
		String value = "";
	    if (file != null)
	    {
	    	value = file.getPath();
	    }
	    
		return value;
	}

	@Override
	public String textParameter3()
	{
		return "Nom du station";
	}
}
