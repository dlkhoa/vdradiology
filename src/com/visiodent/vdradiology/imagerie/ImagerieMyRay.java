package com.visiodent.vdradiology.imagerie;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieMyRay extends ImagerieApplication
{
	public ImagerieMyRay()
	{
		name = Constants.Imagerie_MyRay;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		String PathExe_MyRay = getParameter1();
		String Date_naissance_MyRay = webParameters.DateNaissance.replace("-", ",");
        String Sex_MyRay = webParameters.SexePatient.substring(0, 1);
		/*String PatientPathDirectory_MyRay = Path.split(";")[1];
		File PatientsDirectory_MyRay = new File(PatientPathDirectory_MyRay);

		if (!PatientsDirectory_MyRay.exists()) {
			if (PatientsDirectory_MyRay.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
			{
				System.out.println("Repertoire Patient Cr�e");
			} else {
				System.out.println("Erreur lors de la creation du repertoire patient");
			}
		}*/
		PathExe_MyRay = "\"" + PathExe_MyRay + "\"";

		//MyExe = PathExe_MyRay + " /patid " + Identifiant_patient + " /name " + "\"" + PrenomPatient + "\""
		//		+ " /surname " + "\"" + NomPatient + "\"" + " /dateb " + Date_naissance + " /dir "
		//		+ PatientsDirectory_MyRay;
		this.executable = PathExe_MyRay +
				" /patid " + webParameters.IdentifiantPatient +
				" /name " + "\"" + webParameters.PrenomPatient + "\"" +
				" /surname " + "\"" + webParameters.NomPatient + "\"" +
				" /dateb " + Date_naissance_MyRay +
				" /sex " + Sex_MyRay;

	}

	@Override
	public boolean visibleParameter2()
	{
		return true;			   								   					
	}
}
