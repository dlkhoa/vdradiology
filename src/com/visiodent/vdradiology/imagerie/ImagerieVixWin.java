package com.visiodent.vdradiology.imagerie;

import java.io.File;

import org.apache.commons.lang.StringUtils;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieVixWin extends ImagerieApplication
{
	public ImagerieVixWin()
	{
		name = Constants.Imagerie_VixWin;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		String PathExe_VixWin = getParameter1();
		String PatientPathDirectory_VixWin = getParameter2();
		File PatientsDirectory_VixWin = new File(PatientPathDirectory_VixWin);
		String IDPatient_VixWin = "";

		if (!PatientsDirectory_VixWin.exists()) {
			if (PatientsDirectory_VixWin.mkdir()) /// Cr�ation du Repertoire Radiologie s'il n'existe pas
			{
				System.out.println("Repertoire Patient Cr�e");
			} else {
				System.out.println("Erreur lors de la creation du repertoire patient");
			}
		}

		if (Character.toLowerCase(webParameters.IdentifiantPatient.charAt(0)) == 'o') {
			IDPatient_VixWin = webParameters.IdentifiantPatient.substring(1);
			if (IDPatient_VixWin.length() < 6) {
				IDPatient_VixWin = StringUtils.leftPad(IDPatient_VixWin, 6, "0"); // M�thode disponible dans la
																					// biblioth�que externe
																					// "org.apache.commons.lang.StringUtils"
			}
		} else if (Character.toLowerCase(webParameters.IdentifiantPatient.charAt(0)) == 'j') {
			IDPatient_VixWin = webParameters.IdentifiantPatient.substring(1);
		} else {
			IDPatient_VixWin = webParameters.IdentifiantPatient;
		}

		PathExe_VixWin = "\"" + PathExe_VixWin + "\"";
		this.executable = PathExe_VixWin + " -I " + IDPatient_VixWin + " -N " + "\"" + webParameters.PrenomPatient + "\"" + "^" + "\""
				+ webParameters.NomPatient + "\"";
	}

	@Override
	public boolean visibleParameter2()
	{
		return true;			   								   					
	}
}
