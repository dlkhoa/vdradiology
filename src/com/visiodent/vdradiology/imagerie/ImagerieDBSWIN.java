package com.visiodent.vdradiology.imagerie;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieDBSWIN extends ImagerieApplication
{
	public ImagerieDBSWIN()
	{
		name = Constants.Imagerie_DBSWIN;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		File PatImport = new File("C:\\DBSWIN\\patimport.txt");
		try {
			if (!PatImport.exists())
				PatImport.createNewFile();

			// SimpleDateFormat sm_Durr = new SimpleDateFormat("ddmmyyyy");
			SimpleDateFormat sm_Durr = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Durr = sm_Durr.parse(webParameters.DateNaissance);
			SimpleDateFormat sdfDestination_Durr = new SimpleDateFormat("dd.MM.yyyy");
			String DateNaissance = sdfDestination_Durr.format(DateNaissance_Durr);

			FileWriter PatImportWriter = new FileWriter(PatImport, false);
			/*
			 * PatImportWriter.write(NomPatient + ";" + PrenomPatient + ";" + Date_naissance
			 * + ";" + NumSecu + ";" + ville + ";" + adresse + ";" + numero_fax + ";" +
			 * medecin + ";" + Sexe + ";" + Identifiant_patient + ";");
			 */

			PatImportWriter.write(
					webParameters.NomPatient + ";" + webParameters.PrenomPatient + ";" + DateNaissance + ";" + webParameters.IdentifiantPatient + ";");
			PatImportWriter.close();

		} catch (IOException e) {
			e.printStackTrace();
		} catch (ParseException e) {
			e.printStackTrace();
		}
		this.executable = "\"" + getParameter1() + "\"";
	}
}
