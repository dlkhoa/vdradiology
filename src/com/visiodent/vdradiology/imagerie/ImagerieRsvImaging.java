package com.visiodent.vdradiology.imagerie;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.ini4j.InvalidFileFormatException;
import org.ini4j.Wini;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.Utils;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieRsvImaging extends ImagerieApplication
{
	public ImagerieRsvImaging()
	{
		name = Constants.Imagerie_RSVImaging;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		try {
			//// Formatage de la date en format dd/mm/yyyy
			SimpleDateFormat sm_VisioDent = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_VisioDent = sm_VisioDent.parse(webParameters.DateNaissance);
			SimpleDateFormat sdfDestination_VisioDent = new SimpleDateFormat("dd/MM/yyyy");
			String DateNaissance = sdfDestination_VisioDent.format(DateNaissance_VisioDent);
	
			File AthenaFile = new File("C:\\Windows\\Athena.ini");
	
			// java.util.prefs.Preferences prefs = new IniPreferences(ini); // permet de
			// lire dans un fichier .Ini
	
			// Librairie externe (ini4j) Import�e dans le projet, elle permet de lire et
			// ecrire dans un fichier ini
			Wini ini;
			if (!AthenaFile.exists())
				AthenaFile.createNewFile();

			ini = new Wini(AthenaFile); // On associe notre fichier Athena � l'objet Ini
			ini.put("PATIENT", "Number", webParameters.IdentifiantPatient); // Modification des atributs on acc�dant � la section
																// (Ex:PATIENT)
			ini.put("PATIENT", "Name", webParameters.NomPatient + " " + webParameters.PrenomPatient);
			ini.put("PATIENT", "BirthDate", DateNaissance);
			ini.store();
		}
		catch (InvalidFileFormatException e1) {
			e1.printStackTrace();
		}
		catch (IOException e1) {
			e1.printStackTrace();
		}
		catch (ParseException e1) {
			e1.printStackTrace();
		}
		String path = "\"" + getParameter1() + "\"";
		this.executable = path;
	}
}
