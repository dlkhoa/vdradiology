package com.visiodent.vdradiology.imagerie;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.Utils;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieVatech extends ImagerieApplication
{
	public ImagerieVatech()
	{
		name = Constants.Imagerie_Vatech;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		String executablePath = "\"" + getParameter1() + "\"";
		String directoryPath = Utils.getDirectoryPath(getParameter1(), "bin" + File.separator);
		String patientCreatePath = directoryPath + "crtpatientcreate.xml";
		String patientSearchPath = directoryPath + "crtpatientsearch.xml";
		String patientResultPath = directoryPath + "crtpatientresult.xml";
		String resultCreationPath = directoryPath + "resultcreation.xml";

		File filePatientCreate = new File(patientCreatePath);
		filePatientCreate.delete();
		File filePatientSearch = new File(patientSearchPath);
		filePatientSearch.delete();
		File filePatientResult = new File(patientResultPath);
		filePatientResult.delete();
		File fileResultCreation = new File(resultCreationPath);
		fileResultCreation.delete();
		
		String DateNaissance = "";
		try {
			SimpleDateFormat sm_Vatech = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Vatech = sm_Vatech.parse(webParameters.DateNaissance);
			SimpleDateFormat sdfDestination_Vatech = new SimpleDateFormat("MM-dd-yyyy");
			DateNaissance = sdfDestination_Vatech.format(DateNaissance_Vatech);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		String fichierxmlin = 
			"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
			"<Request>\n" + 
			"<SearchPatient>\n"	+
			"<Search ChartNo=\"" + webParameters.IdentifiantPatient + "\"/>\n" + 
			"</SearchPatient>\n" + 
			"</Request>\n";
		
		try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(patientSearchPath), "utf-8")))
		{
			   writer.write(fichierxmlin);
		} catch (UnsupportedEncodingException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Runtime Run = Runtime.getRuntime();

		try {
			Process p1 = Run.exec(executablePath + " /in:\"" + patientSearchPath + "\" /out:\"" + patientResultPath + "\"");
		    p1.waitFor();
		} catch (IOException e1) {
			e1.printStackTrace();
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		try (BufferedReader br = new BufferedReader(new FileReader(patientResultPath))) {
		    StringBuilder sb = new StringBuilder();
		    String crtline = br.readLine();

		    while (crtline != null) {
		        sb.append(crtline);
		        sb.append(System.lineSeparator());
		        crtline = br.readLine();
		    }
		    String everything = sb.toString();
		    
			if (everything.toLowerCase().contains("no such data in db")) {
				String newpatientxml = 
					"<?xml version=\"1.0\" encoding=\"UTF-8\"?>\n" + 
					"<Request>\n" + 
					"<CreatePatient>\n" + 
					"<Patient ChartNo=\"" + webParameters.IdentifiantPatient + 
						"\" FirstName=\"" + webParameters.PrenomPatient +
						"\" LastName=\"" + webParameters.NomPatient +
						"\" Gender=\"" + webParameters.SexePatient + "\"\n" + 
						"Birthdate=\""+DateNaissance+"\"/>\n" + 
					"</CreatePatient>\n" + 
					"</Request>";
				
				try (Writer writer = new BufferedWriter(new OutputStreamWriter(new FileOutputStream(patientCreatePath), "utf-8"))) {
					   writer.write(newpatientxml);
				} catch (UnsupportedEncodingException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (FileNotFoundException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				
				this.executable = executablePath + " /in:\"" + patientCreatePath + "\" /out:\"" + resultCreationPath + "\" /run:" + webParameters.IdentifiantPatient;
			}
			else {
				this.executable = executablePath + " /run:" + webParameters.IdentifiantPatient;
			}
		    
		} catch (FileNotFoundException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
	}
}
