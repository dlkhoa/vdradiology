package com.visiodent.vdradiology.imagerie;

import java.io.File;

import com.visiodent.vdradiology.Utils;
import com.visiodent.vdradiology.WebParameters;

import javafx.stage.DirectoryChooser;
import javafx.stage.FileChooser;
import javafx.stage.Window;

public class ImagerieApplication
{
	protected String name;
	protected String category;
	
	protected String parameter1;
	protected String parameter2;
	protected String parameter3;

	protected boolean loaded = false;

	protected String executable;

	public String getCategory()
	{
		return category;
	}

	public void setCategory(String value)
	{
		category = value;
	}

	public String getName()
	{
		return name;
	}

	public void setName(String value)
	{
		name = value;
	}

	public String getParameter1()
	{
		return parameter1;
	}

	public void setParameter1(String value)
	{
		parameter1 = value;
	}

	public String getParameter2()
	{
		return parameter2;
	}

	public void setParameter2(String value)
	{
		parameter2 = value;
	}

	public String getParameter3()
	{
		return parameter3;
	}

	public void setParameter3(String value)
	{
		parameter3 = value;
	}

	public boolean getLoaded()
	{
		return loaded;
	}

	public void setLoaded(boolean value)
	{
		loaded = value;
	}

	public void handle(WebParameters webParameters)
	{
		prepare(webParameters);
		execute();
	}

	protected void prepare(WebParameters webParameters)
	{
	}

	protected void execute()
	{
		try
		{
			Runtime Run = Runtime.getRuntime();
			Run.exec(executable);
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	public boolean visibleParameter1()
	{
		return true;
	}

	public String textParameter1()
	{
		return "Fichier executable ad�quat";
	}

	public String textButtonParameter1()
	{
		return "Parcourir...";
	}

	public String defineParameter1(Window parentWindow)
	{
		FileChooser chooser = new FileChooser();
		chooser.setTitle(textParameter1());
		File file = chooser.showOpenDialog(parentWindow);
		String value = "";
	    if (file != null)
	    {
	    	value = file.getPath();			         
	    }
	    
		return value;
	}

	public boolean visibleParameter2()
	{
		return false;
	}

	public String textParameter2()
	{
		return "R�pertoire de sauvegarde radios patients";
	}

	public String textButtonParameter2()
	{
		return "Parcourir...";
	}

	public String defineParameter2(Window parentWindow)
	{
		DirectoryChooser dirChooser = new DirectoryChooser(); 
		dirChooser.setTitle(textParameter2());
		File file = dirChooser.showDialog(parentWindow);
		String value = "";
	    if (file != null)
	    {
	    	value = file.getPath();			         
	    }
	    
		return value;
	}

	public boolean visibleParameter3()
	{
		return false;
	}

	public String textParameter3()
	{
		return "";
	}

	public String serialize()
	{
		String settingLine = category + "=" + name + "=" + parameter1;
		if (visibleParameter2())
		{
			settingLine += ";" + parameter2;
		}
		if (visibleParameter3())
		{
			settingLine += ";" + parameter3;
		}

		return settingLine;
	}
}
