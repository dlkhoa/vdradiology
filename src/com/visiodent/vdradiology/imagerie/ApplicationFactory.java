package com.visiodent.vdradiology.imagerie;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.visiodent.vdradiology.Constants;

public class ApplicationFactory
{
	private static Map<String, ImagerieApplication> listApplications = null;
	private static List<String> listApplicationNames = null;

	public static Map<String, ImagerieApplication> getApplicationList()
	{
		return getApplicationList(false);
	}

	public static Map<String, ImagerieApplication> getApplicationList(boolean loaded)
	{
		initStaticLists();
		
		if (loaded)
		{
			return listApplications
					.entrySet()
					.stream()
					.filter(x -> x.getValue().getLoaded())
					.collect(Collectors.toMap(x -> x.getKey(), x -> x.getValue()));
		}

		return listApplications;
	}

	public static List<String> getApplicationNames()
	{
		initStaticLists();

		return listApplicationNames;
	}

	public static ImagerieApplication getImagerieApplication(String applicationName)
	{
		initStaticLists();

		return listApplications.get(applicationName);
	}

	public static void loadApplicationList()
	{
		initStaticLists();

		try {
			BufferedReader settingFileBuffer = new BufferedReader(new FileReader(Constants.SettingFile));
			String settingLine;
			while ((settingLine = settingFileBuffer.readLine()) != null) {
				updateImagerieApplication(settingLine);
			}
			settingFileBuffer.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void updateImagerieApplication(
			String applicationName, String category, 
			String parameter1, String parameter2, String parameter3)
	{
		ImagerieApplication imagerieApplication = getImagerieApplication(applicationName);
		if (imagerieApplication != null)
		{
			imagerieApplication.setCategory(category);
			imagerieApplication.setParameter1(parameter1);
			imagerieApplication.setParameter2(parameter2);
			imagerieApplication.setParameter3(parameter3);
			imagerieApplication.setLoaded(true);
			listApplications.put(applicationName, imagerieApplication);
		}
	}

	public static void saveApplicationList()
	{
		try
		{
			FileWriter settingFileWriter = new FileWriter(Constants.SettingFile);
			for (String applicationName: listApplicationNames)
			{
				ImagerieApplication imagerieApplication = getImagerieApplication(applicationName);
				if (imagerieApplication != null && imagerieApplication.getLoaded())
				{
					settingFileWriter.write(imagerieApplication.serialize() + System.lineSeparator());
				}
			}
			settingFileWriter.close();	   					   
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}

	private static void initStaticLists()
	{
		if (listApplicationNames == null)
		{
			listApplicationNames = new ArrayList<String>();
			listApplicationNames.add(Constants.Imagerie_CSImaging);
			listApplicationNames.add(Constants.Imagerie_RSVImaging);
			listApplicationNames.add(Constants.Imagerie_Romexis);
			listApplicationNames.add(Constants.Imagerie_DBSWIN);
			listApplicationNames.add(Constants.Imagerie_Scanora);
			listApplicationNames.add(Constants.Imagerie_CliniView);
			listApplicationNames.add(Constants.Imagerie_MyRay);
			listApplicationNames.add(Constants.Imagerie_VixWin);
			listApplicationNames.add(Constants.Imagerie_QuickVision);
			listApplicationNames.add(Constants.Imagerie_Sirona);
			listApplicationNames.add(Constants.Imagerie_Vatech);
			listApplicationNames.add(Constants.Imagerie_DFW);
			listApplicationNames.add(Constants.Imagerie_Sopro);
		}

		if (listApplications == null)
		{
			listApplications = new HashMap<String, ImagerieApplication>();
			for (String applicationName: listApplicationNames)
			{
				ImagerieApplication imagerieApplication = createImagerieApplication(applicationName);
				if (imagerieApplication != null)
				{
					listApplications.put(applicationName, imagerieApplication);
				}
			}
		}
	}

	private static ImagerieApplication createImagerieApplication(String applicationName) {
		ImagerieApplication imagerieApplication = null;

		switch (applicationName)
		{
		case Constants.Imagerie_CSImaging:
			imagerieApplication = new ImagerieCSImaging();
			break;
		case Constants.Imagerie_RSVImaging:
			imagerieApplication = new ImagerieRsvImaging();
			break;
		case Constants.Imagerie_Romexis:
			imagerieApplication = new ImagerieRomexis();
			break;
		case Constants.Imagerie_DBSWIN:
			imagerieApplication = new ImagerieDBSWIN();
			break;
		case Constants.Imagerie_Scanora:
			imagerieApplication = new ImagerieScanora();
			break;
		case Constants.Imagerie_CliniView:
			imagerieApplication = new ImagerieCliniView();
			break;
		case Constants.Imagerie_MyRay:
			imagerieApplication = new ImagerieMyRay();
			break;
		case Constants.Imagerie_VixWin:
			imagerieApplication = new ImagerieVixWin();
			break;
		case Constants.Imagerie_QuickVision:
			imagerieApplication = new ImagerieQuickVision();
			break;
		case Constants.Imagerie_Sirona:
			imagerieApplication = new ImagerieSirona();
			break;
		case Constants.Imagerie_Vatech:
			imagerieApplication = new ImagerieVatech();
			break;
		case Constants.Imagerie_DFW:
			imagerieApplication = new ImagerieDFW();
			break;
		case Constants.Imagerie_Sopro:
			imagerieApplication = new ImagerieSopro();
			break;
		default:
			break;
		}
		
		return imagerieApplication;
	}

	private static void updateImagerieApplication(String settingLine)
	{
		String[] parts = settingLine.split("=");
		if (parts != null && parts.length > 2)
		{
			String category = parts[0];
			String name = parts[1];
			String parameters = parts[2];
			String parameter1 = "";
			String parameter2 = "";
			String parameter3 = "";
			String[] parameterParts = parameters.split(";");
			if (parameterParts != null)
			{
				if (parameterParts.length > 0)
				{
					parameter1 = parameterParts[0];
				}
				if (parameterParts.length > 1)
				{
					parameter2 = parameterParts[1];
				}
				if (parameterParts.length > 2)
				{
					parameter3 = parameterParts[2];
				}
			}
			updateImagerieApplication(name, category, parameter1, parameter2, parameter3);
		}
	}
}
