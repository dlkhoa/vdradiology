package com.visiodent.vdradiology.imagerie;

import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.Utils;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieDFW extends ImagerieApplication
{
	public ImagerieDFW()
	{
		name = Constants.Imagerie_DFW;
	}

	@Override
	public void prepare(WebParameters webParameters)
	{
		String name = webParameters.NomPatient + " " + webParameters.PrenomPatient;
		String code = webParameters.IdentifiantPatient;
		String command = "$$DFWIN$$ OPEN -n\"" + name + "\" -c\"" + code + "\" -b -r -a";
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard();
	    Transferable transferable = new StringSelection(command);
	    clipboard.setContents(transferable, null);
		String path = "\"" + getParameter1() + "\"";
		this.executable = path;
	}

	@Override
	public boolean visibleParameter2()
	{
		return false;			   								   					
	}

	@Override
	public boolean visibleParameter3()
	{
		return false;
	}
}
