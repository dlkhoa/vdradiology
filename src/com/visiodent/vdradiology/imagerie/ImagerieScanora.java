package com.visiodent.vdradiology.imagerie;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieScanora extends ImagerieApplication
{
	public ImagerieScanora()
	{
		name = Constants.Imagerie_Scanora;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		try {
			String Path = "\"" + getParameter1() + "\"";
			SimpleDateFormat sm_Scanora = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Scanora = sm_Scanora.parse(webParameters.DateNaissance);
			SimpleDateFormat sdfDestination_Scanora = new SimpleDateFormat("dd/MM/yyyy");
			String DateNaissance = sdfDestination_Scanora.format(DateNaissance_Scanora);
			this.executable = Path + " -" + webParameters.IdentifiantPatient + ";\"" + webParameters.PrenomPatient + "\";\"" + webParameters.NomPatient + "\";;"
					+ DateNaissance + ";";
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
	}
}
