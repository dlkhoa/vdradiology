package com.visiodent.vdradiology.imagerie;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.visiodent.vdradiology.Constants;
import com.visiodent.vdradiology.WebParameters;

public class ImagerieCliniView extends ImagerieApplication
{
	public ImagerieCliniView()
	{
		name = Constants.Imagerie_CliniView;
	}

	@Override
	public void prepare(WebParameters webParameters) {
		try {
			String Path = "\"" + getParameter1() + "\"";
			SimpleDateFormat sm_Clini = new SimpleDateFormat("dd-MM-yyyy");
			Date DateNaissance_Clini = sm_Clini.parse(webParameters.DateNaissance);
			SimpleDateFormat sdfDestination_Clini = new SimpleDateFormat("dd/MM/yyyy");
			String DateNaissance = sdfDestination_Clini.format(DateNaissance_Clini);
			this.executable = Path + " -" + webParameters.IdentifiantPatient + ";\"" + webParameters.PrenomPatient + "\";\"" + webParameters.NomPatient + "\";;"
					+ DateNaissance + ";";
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
