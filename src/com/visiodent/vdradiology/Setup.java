package com.visiodent.vdradiology;

import java.io.IOException;

import com.visiodent.vdradiology.imagerie.ApplicationFactory;
import com.visiodent.vdradiology.imagerie.ImagerieApplication;

import javafx.application.Application;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

public class Setup extends Application
{
	private Stage primaryStage;
	private Scene sceneCategory;
	private Scene sceneParam;
	
	private String selectedCategory;
	private ImagerieApplication selectedApplication;

	public static void main(String[] args) 
	{
        ApplicationFactory.loadApplicationList();
		launch(args);
	}

	public void start(Stage primaryStage) throws IOException 
	{
		initSceneCategory();
		initSceneParam();
		initPrimaryStage(primaryStage);
	}
	
	private void initPrimaryStage(Stage stage)
	{
		primaryStage = stage;
		Image Icon = new Image(Utils.getResourceStream(getClass(), Constants.Resource_Logo2));
		primaryStage.getIcons().add(Icon);
		primaryStage.setResizable(false);
		primaryStage.show();
		setScene(sceneCategory, Constants.TitleCategory, Constants.SceneCategory_Width, Constants.SceneCategory_Height);
	}
	
	private void initSceneCategory()
	{
		Button btnCapteur = createCategoryButton(Constants.Categorie_Capteur, Constants.Resource_Capteur);
		Button btnPanoramique = createCategoryButton(Constants.Categorie_Panoramique, Constants.Resource_Panoramique);
		Button btnCamera = createCategoryButton(Constants.Categorie_Camera, Constants.Resource_Camera);
		ImageView imageLogo = createCategoryImage(Constants.Resource_Logo1);
		Label labelCategory  = new Label("Choix de la cat�gorie");
 		
		GridPane gridCategory = new GridPane();
		gridCategory.setStyle("-fx-background-color: AliceBlue;");
		gridCategory.setHgap(Constants.SceneCategory_Gap);
		gridCategory.setVgap(Constants.SceneCategory_Gap);
		gridCategory.add(labelCategory, 2, 1);			
		gridCategory.add(btnCapteur, 2, 3);
		gridCategory.add(btnPanoramique, 2, 4);		
		gridCategory.add(btnCamera, 3, 3);	
		gridCategory.add(imageLogo, 3, 4);

		sceneCategory = new Scene(gridCategory, Constants.SceneCategory_Width, Constants.SceneCategory_Height);
		sceneCategory.getStylesheets().add(Utils.getResourceString(getClass(), Constants.Resource_Css));
	}

	private void initSceneParam()
	{
		Label labelParameter1 = new Label("Parameter1");

		TextField textParameter1 = new TextField ();
		textParameter1.setPrefSize(Constants.ParamText_Width, Constants.ParamText_Height);
		
		Button btnParameter1 = new Button("Set parameter1");		
		btnParameter1.setPrefSize(Constants.ParamButton_Width, Constants.ParamButton_Height);
		btnParameter1.setOnAction(actionEvent -> 
		{ 				
			if (selectedApplication != null)
			{
				String value = selectedApplication.defineParameter1(primaryStage.getScene().getWindow());
				if (value != "")
				{
					textParameter1.setText(value);
				}
			}
		});
		
		Label labelParameter2 = new Label("Parameter2");

		TextField textParameter2 = new TextField ();
		textParameter2.setPrefSize(Constants.ParamText_Width, Constants.ParamText_Height);

		Button btnParameter2 = new Button("Set parameter2");		
		btnParameter2.setPrefSize(Constants.ParamButton_Width, Constants.ParamButton_Height);
		btnParameter2.setOnAction(actionEvent -> 
		{ 				
			if (selectedApplication != null)
			{
				String value = selectedApplication.defineParameter2(primaryStage.getScene().getWindow());
				if (value != "")
				{
					textParameter2.setText(value);
				}
			}
		});

		Label labelParameter3 = new Label("Parameter3");
		TextField textParameter3 = new TextField ();
		textParameter3.setPrefSize(Constants.ParamText_Width, Constants.ParamText_Height);

		Label labelApplication = new Label("Logiciel");

		ObservableList<String> listApplications = FXCollections.observableArrayList(ApplicationFactory.getApplicationNames());
		ComboBox<String> comboApplication = new ComboBox<String>(listApplications);
		comboApplication.setPrefSize(Constants.ParamCombo_Width, Constants.ParamCombo_Height);
		comboApplication.getSelectionModel().selectFirst();
		comboApplication.setOnAction((event) ->
		{
			String applicationName = comboApplication.getSelectionModel().getSelectedItem();
			selectedApplication = ApplicationFactory.getImagerieApplication(applicationName);
			
			if (selectedApplication != null)
			{
				labelParameter1.setVisible(selectedApplication.visibleParameter1());
				labelParameter1.setText(selectedApplication.textParameter1());
				textParameter1.setVisible(selectedApplication.visibleParameter1());
				textParameter1.setText(selectedApplication.getParameter1());
				btnParameter1.setVisible(selectedApplication.visibleParameter1());
				btnParameter1.setText(selectedApplication.textButtonParameter1());
	
				labelParameter2.setVisible(selectedApplication.visibleParameter2());
				labelParameter2.setText(selectedApplication.textParameter2());
				textParameter2.setVisible(selectedApplication.visibleParameter2());
				textParameter2.setText(selectedApplication.getParameter2());
				btnParameter2.setVisible(selectedApplication.visibleParameter2());
				btnParameter2.setText(selectedApplication.textButtonParameter1());
	
				labelParameter3.setVisible(selectedApplication.visibleParameter3());
				labelParameter3.setText(selectedApplication.textParameter3());
				textParameter3.setVisible(selectedApplication.visibleParameter3());
				textParameter3.setText(selectedApplication.getParameter3());
			}
		});
		comboApplication.fireEvent(new ActionEvent());

		Button btnReturn = new Button("Pr�c�dent");
		btnReturn.setPrefSize(Constants.ParamButton_Width, Constants.ParamButton_Height);
		btnReturn.setOnAction(actionEvent -> 
		{ 				
			setScene(sceneCategory, Constants.TitleCategory, Constants.SceneCategory_Width, Constants.SceneCategory_Height);
		});

		Button btnApply = new Button("Ex�cuter");
		btnApply.setPrefSize(Constants.ParamButton_Width, Constants.ParamButton_Height);
		btnApply.setOnAction(actionEvent -> 
		{
			ApplicationFactory.updateImagerieApplication(
					selectedApplication.getName(), selectedCategory,
					textParameter1.getText(), textParameter2.getText(), textParameter3.getText());
	        ApplicationFactory.saveApplicationList();
			setScene(sceneCategory, Constants.TitleCategory, Constants.SceneCategory_Width, Constants.SceneCategory_Height);
	        Utils.showDialog(Constants.TitleInformation, "Syst�me d'imagerie mis � jour!");
		});			

		GridPane gridParam = new GridPane();

		gridParam.add(labelApplication, 0, 2);		
		gridParam.add(comboApplication, 1, 2);

		gridParam.add(labelParameter1, 0, 3);
		gridParam.add(textParameter1, 1, 3);		
		gridParam.add(btnParameter1, 2, 3);
		gridParam.add(labelParameter2, 0, 4);
		gridParam.add(textParameter2, 1, 4);		
		gridParam.add(btnParameter2, 2, 4);
		gridParam.add(labelParameter3, 0, 5);
		gridParam.add(textParameter3, 1, 5);		

		gridParam.add(btnApply, 2, 10);
		gridParam.add(btnReturn, 2, 15);

		gridParam.setAlignment(Pos.BASELINE_CENTER);
		gridParam.setHgap(Constants.SceneParam_Gap);
		gridParam.setVgap(Constants.SceneParam_Gap);
		sceneParam = new Scene(gridParam, Constants.SceneParam_Width, Constants.SceneParam_Height);
		sceneParam.getStylesheets().add(Utils.getResourceString(getClass(), Constants.Resource_Css));
	}

	private Button createCategoryButton(String categoryName, String resourceName)
	{
		Image image = new Image(Utils.getResourceStream(getClass(), resourceName));
		ImageView imageView = new ImageView(image);
		imageView.setFitHeight(Constants.CategoryButton_FitHeight);
		imageView.setFitWidth(Constants.CategoryButton_FitWidth);
		Button btn = new Button();
		btn.setGraphic(imageView);
		btn.setMinSize(Constants.CategoryButton_FitWidth, Constants.CategoryButton_FitHeight);
		btn.setOnAction(actionEvent -> 
		{	
			selectedCategory = categoryName;
			setScene(sceneParam, Constants.TitleParam + categoryName, Constants.SceneParam_Width, Constants.SceneParam_Height);
		});
		
		return btn;
    }

	private ImageView createCategoryImage(String resourceName)
	{
		Image image = new Image(Utils.getResourceStream(getClass(), resourceName));
		ImageView imageView = new ImageView(image);
		imageView.setFitHeight(Constants.CategoryButton_FitHeight);
		imageView.setFitWidth(Constants.CategoryButton_FitWidth);
		
		return imageView;
    }

	private void setScene(Scene scene, String title, int width, int height)
	{
		primaryStage.setScene(scene);
		primaryStage.setWidth(width);
		primaryStage.setHeight(height);
		primaryStage.setTitle(title);
	}
}
